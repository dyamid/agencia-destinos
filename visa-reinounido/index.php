<!DOCTYPE html>
<html>
<head>
  <?php $base_url = "https://agenciadestinos.net/" ?>

<!-- /.website title -->
<title>Visa Del Reino Unido</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<!-- CSS Files -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">

<!-- Colors -->
<link href="css/css-index.css" rel="stylesheet" media="screen">
<!-- <link href="css/css-index-green.css" rel="stylesheet" media="screen"> -->
<!-- <link href="css/css-index-purple.css" rel="stylesheet" media="screen"> -->
<!-- <link href="css/css-index-red.css" rel="stylesheet" media="screen"> -->
<!-- <link href="css/css-index-orange.css" rel="stylesheet" media="screen"> -->
<!-- <link href="css/css-index-yellow.css" rel="stylesheet" media="screen"> -->

<!-- Google Fonts -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic" />

</head>
 
<body data-spy="scroll" data-target="#navbar-scroll">

<!-- /.preloader -->


<div id="top"></div>
<!-- /.parallax full screen background image -->
<div class="fullscreen landing parallax" style="background-image:url('images/bg2.jpg');" data-img-width="2000" data-img-height="1333" data-diff="100">
	
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
				
					<!-- /.logo -->
					<div class="logo wow fadeInDown"> <a href=""><img src="../asesoria-visa-americana/images/logo2.png" alt="logo"></a></div>

					<!-- /.main title -->
						<h1 class="wow fadeInLeft">
						ASESORIA DE VISADOS DEL REINO UNIDO
						</h1>

					<!-- /.header paragraph -->
					<div class="landing-text wow fadeInUp">
						<p>Tramite su visa de manera virtual</p>
						<p>Conocimientos sobre el nuevo sistema del vfs global</p>
						<p>Diligenciamos su tramite, agendamos su cita y realizamos asesoria las 24 horas.</p>
						<h3 style="color:white"><strong>Contactenos Ahora! 320 419 9366</strong></h3>
					</div>				  

					<!-- /.header button -->
					<div class="head-btn wow fadeInLeft">
						<a href="#feature" class="btn-primary">Visados</a>
						<a href="#download" class="btn-default">Contactenos</a>
					</div>
	
		  

				</div> 
				
				<!-- /.signup form -->
				<div class="col-md-5">
				
					<!-- <div class="signup-header wow fadeInUp">
						<h3 class="form-title text-center">RECIBIR INFORMACION</h3>
						<form class="form-header" action="" role="form" method="POST" id="#">
						<input type="hidden" name="u" value="503bdae81fde8612ff4944435">
						<input type="hidden" name="id" value="bfdba52708">
							<div class="form-group">
								<input class="form-control input-lg" name="MERGE1" id="name" type="text" placeholder="Nombre" required>
							</div>
							<div class="form-group">
								<input class="form-control input-lg" name="MERGE0" id="email" type="email" placeholder="Correo Electronico" required>
							</div>
							<div class="form-group last">
								<input type="submit" class="btn btn-warning btn-block btn-lg" value="Enviar">
							</div>
							<p class="privacy text-center">Nosotros no compartimos su email. <a href="#">Poiliticas de Privacidad</a>.</p>
						</form>
					</div> -->				
				
				</div>
			</div>
		</div> 
	</div> 
</div>
 
<!-- NAVIGATION -->
<div id="menu">
	<nav class="navbar-wrapper navbar-default" role="navigation">
		<div class="container">
			  <div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-backyard">
				  <span class="sr-only">Navegacion</span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand site-name" href="#top"><img src="../asesoria-visa-americana/images/logo2.png" alt="logo"></a>
			  </div>
	 
			  <div id="navbar-scroll" class="collapse navbar-collapse navbar-backyard navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="#intro">Nosotros</a></li>
					<li><a href="#feature">Visas</a></li>
					<li><a href="#download">Asesoria</a></li>
					<!-- <li><a href="#package">Saber mas</a></li> -->
					<li><a href="#testi">Testimonios</a></li>
					<li><a href="#contact">Contactenos</a></li>
				</ul>
			  </div>
		</div>
	</nav>
</div>

<!-- /.intro section -->
<div id="intro">
	<div class="container">
		<div class="row">

		<!-- /.intro image -->
			<div class="col-md-6 intro-pic wow slideInLeft">
				<img src="images/visados.jpg" alt="image" class="img-responsive">
			</div>	
			
			<!-- /.intro content -->
			<div class="col-md-6 wow slideInRight">
				<h2>Agencia destinos especialistas en asesoria de visas</h2>
				<p>Nuestra empresa cuenta con un número de asesores, los cuales están capacitados para realizar una asesoría completa y eficaz. <br>
				Estudio de documentacion para saber si es elegible o no. Diligenciamiento de formularios online o con un asesor.	Asesoría para la documentación. Asesoría para las fotos. Asesoria para la toma de datos biometricos. Recibo de pago para sus derechos consulares.
				</p>

					<div class="btn-section"><a href="#feature" class="btn-default">Leer más</a></div>
		
			</div>
		</div>			  
	</div>
</div>

<!-- /.feature section -->
<div id="feature">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-12 text-center feature-title">

			<!-- /.feature title -->
				<h2>Visados para el Reino Unido</h2>
				<p>En agencia destinos le decimos que tipo de visado necesita para viajar al Reino Unido y le ofrecemos asesoria sobre ello.</p>
			</div>
		</div>
		<div class="row row-feat">
			<div class="col-md-4 text-center">
			
			<!-- /.feature image -->
				<div class="feature-img">
					<img src="images/tipo-visado.jpg" alt="image" class="img-responsive wow fadeInLeft">
				</div>
			</div>
		
			<div class="col-md-8">
			
				<!-- /.feature 1 -->
				<div class="col-sm-6 feat-list">
					<i class="pe-7s-notebook pe-5x pe-va wow fadeInUp"></i>
					<div class="inner">
						<h4>Visa de turismo o visitante</h4>
						<p>La visa de turismo o visitante estándar se otorga a las personas que desean viajar al reino unido para el ocio, como vacaciones o turismo y para ver a su familia y amigos.
						</p>
					</div>
				</div>
			
				<!-- /.feature 2 -->
			
				<!-- /.feature 3 -->
				<div class="col-sm-6 feat-list">
					<i class="pe-7s-cart pe-5x pe-va wow fadeInUp" data-wow-delay="0.4s"></i>
					<div class="inner">
						<h4>Visa De Transito</h4>
						<p>Esta visa la necesitara si va al reino unido solo para realizar transbordo de aeronave o porque su aerolínea y avión hacen una parada en el reino unido.</p>
					</div>
				</div>
				<div class="col-sm-6 feat-list">
					<i class="pe-7s-cash pe-5x pe-va wow fadeInUp" data-wow-delay="0.2s"></i>
					<div class="inner">
						<h4>Visa Estudiante Reino Unido</h4>
						<p>Visa nivel 4 general : esta es por si tiene 16 años o mas y va a estudiar algún curso o o carrera universitaria que dure por un periodo de 6 meses, 11 meses o mas.</p>
					</div>
				</div>
			
				<!-- /.feature 4 -->
				<div class="col-sm-6 feat-list">
					<i class="pe-7s-users pe-5x pe-va wow fadeInUp" data-wow-delay="0.6s"></i>
					<div class="inner">
						<h4>Visa De Trabajo</h4>
						<p>Estas visas son otorgadas para una estancia mayor a 6 meses, es una visa que dan para trabajar hacer negocios o investigación académica en el reino unido.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- /.feature 2 section -->
<div id="feature-2">
	<div class="container">
		<div class="row">
	
			<!-- /.feature content -->
			<div class="col-md-6 wow fadeInLeft">
				<h2>Visados Para el Reino Unido</h2>
				<p>Las visas se otorgan para una estancia máxima de seis meses. Si necesitas una visa y quieres quedarte en el Reino Unido más de seis meses, deberás abandonar el país y solicitar una nueva visa. Si quieres entrar y salir del Reino Unido durante tu estancia, te ahorrarás problemas si solicitas una visa de múltiples entradas. Si tienes una visa para una sola entrada, tendrás que solicitar una nueva cada vez que salgas del país.
				</p>

					<div class="btn-section"><a href="#download" class="btn-default">Saber mas...</a></div>
		
			</div>
			  
			<!-- /.feature image -->
			<div class="col-md-6 feature-2-pic wow fadeInRight">
				<img src="images/visados.jpg" alt="macbook" class="img-responsive">
			</div>				  
		</div>			  
  
	</div>
</div>


<!-- /.download section -->
<div id="download">
	<div class="action fullscreen parallax" style="background-image:url('images/bg.jpg');" data-img-width="2000" data-img-height="1333" data-diff="100">
		<div class="overlay">
			<div class="container">
				<div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
				
					<!-- /.download title -->
					<!-- <h2 class="wow fadeInRight">Asesorias</h2>
					<p class="download-text wow fadeInLeft">
							Nuestra empresa cuenta con un número de asesores, los cuales están capacitados para realizar una asesoría completa y eficaz.
		Estudio de documentacion para saber si es elegible o no. Diligenciamiento de formularios online o con un asesor.	Asesoría para la documentación. Asesoría para las fotos. Asesoria para la toma de datos biometricos. Recibo de pago para sus derechos consulares.
						
					</p> -->
					
							<h2 class="wow fadeInRight">Preguntas Visa Reino Unido</h2>
							<p class="download-text wow fadeInLef">
			    				<?php require __DIR__.'/../asesorias/preguntas/visa-reino-unido-preguntas.php'; ?>
							</p>
					<!-- /.download button -->
						<div class="download-cta wow fadeInLeft">
							<a href="#contact" class="btn-secondary">Contactenos</a>
						</div>
						
				</div>	
			</div>	
		</div>
	</div>
</div>

<!-- /.pricing section -->
<!-- <div id="package">
	<div class="container">
		<div class="text-center">
		

			<h2 class="wow fadeInLeft">PACKAGES</h2>
			<div class="title-line wow fadeInRight"></div>
		</div>
		<div class="row package-option">


			<div class="col-sm-3">
			  <div class="price-box wow fadeInUp">
			   <div class="price-heading text-center">
			   

					<i class="pe-7s-radio pe-5x"></i>
					

					<h3>Basic</h3>
			   </div>
			   
			   
				<div class="price-group text-center">
					<span class="dollar">$</span>
					<span class="price">9</span>
					<span class="time">/mo</span>
				</div>
			
				
			   <ul class="price-feature text-center">
				  <li><strong>100MB</strong> Disk Space</li>
				  <li><strong>200MB</strong> Bandwidth</li>
				  <li><strong>2</strong> Subdomains</li>
				  <li><strong>5</strong> Email Accounts</li>
				  <li><strike>Webmail Support</strike></li>				  
			   </ul>
			   
			   
			   <div class="price-footer text-center">
				 <a class="btn btn-price" href="#">BUY NOW</a>
				</div>	
			  </div>
			</div>
			
			
			<div class="col-sm-3">
			  <div class="price-box wow fadeInUp" data-wow-delay="0.2s">
			   <div class="price-heading text-center">

			   
				<i class="pe-7s-joy pe-5x"></i>

			   
				<h3>Standard</h3>
			   </div>
			   
			   
				<div class="price-group text-center">
					<span class="dollar">$</span>
					<span class="price">19</span>
					<span class="time">/mo</span>
				</div>
			
				
			   <ul class="price-feature text-center">
				  <li><strong>300MB</strong> Disk Space</li>
				  <li><strong>400MB</strong> Bandwidth</li>
				  <li><strong>5</strong> Subdomains</li>
				  <li><strong>10</strong> Email Accounts</li>
				  <li><strike>Webmail Support</strike></li>			  
			   </ul>

				
			   <div class="price-footer text-center">
				 <a class="btn btn-price" href="#">BUY NOW</a>
				</div>
			  </div>
			</div>	
			
			
			<div class="col-sm-3">
			  <div class="price-box wow fadeInUp" data-wow-delay="0.4s">
			   <div class="price-heading text-center">
			   
					
					<i class="pe-7s-science pe-5x"></i>
				
					
					<h3>Advance</h3>
			   </div>
			   
			   
				<div class="price-group text-center">
					<span class="dollar">$</span>
					<span class="price">29</span>
					<span class="time">/mo</span>
				</div>
			
				
			   <ul class="price-feature text-center">
				  <li><strong>1GB</strong> Disk Space</li>
				  <li><strong>1GB</strong> Bandwidth</li>
				  <li><strong>10</strong> Subdomains</li>
				  <li><strong>25</strong> Email Accounts</li>
				  <li>Webmail Support</li>					  
			   </ul>
			   
			   
			   <div class="price-footer text-center">
				 <a class="btn btn-price" href="#">BUY NOW</a>
				</div>	
			  </div>
			</div>
			
			
			<div class="col-sm-3">
			  <div class="price-box wow fadeInUp" data-wow-delay="0.6s">
			   <div class="price-heading text-center">
			   
					
					<i class="pe-7s-tools pe-5x"></i>
					
					
					<h3>Ultimate</h3>
			   </div>
			   
			   
				<div class="price-group text-center">
					<span class="dollar">$</span>
					<span class="price">49</span>
					<span class="time">/mo</span>
				</div>
			
				
			   <ul class="price-feature text-center">
				  <li><strong>5GB</strong> Disk Space</li>
				  <li><strong>5GB</strong> Bandwidth</li>
				  <li><strong>50</strong> Subdomains</li>
				  <li><strong>50</strong> Email Accounts</li>
				  <li>Webmail Support</li>			  
			   </ul>
			   
			   
			   <div class="price-footer text-center">
				 <a class="btn btn-price" href="#">BUY NOW</a>
				</div>
			  </div>
			</div>

		</div>
	</div>
</div> -->
 
<!-- /.client section -->
<!-- <div id="client"> 
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<img alt="client" src="images/company.jpg" class="wow fadeInUp">
					<img alt="client" src="images/company.jpg" class="wow fadeInUp" data-wow-delay="0.2s">
					<img alt="client" src="images/company.jpg" class="wow fadeInUp" data-wow-delay="0.4s">
					<img alt="client" src="images/company.jpg" class="wow fadeInUp" data-wow-delay="0.6s">
				</div>
			</div>
		</div>	
</div> -->

<!-- /.testimonial section -->
<div id="testi">
	<div class="container">
		<div class="text-center">
			<h2 class="wow fadeInLeft">Testimonios</h2>
			<div class="title-line wow fadeInRight"></div>
		</div>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">	
			   <div id="owl-testi" class="owl-carousel owl-theme wow fadeInUp">
				 
					<!-- /.testimonial 1 -->
					<div class="testi-item">
						<div class="client-pic text-center">
						
							<!-- /.client photo -->
							<!-- <img src="images/client.jpg" alt="client"> -->
						</div>
						<div class="box">
						
							<!-- /.testimonial content -->
							<p class="message text-center">"Buenas noches, quiero agradecer la inmensa paciencia que tuvieron conmigo y con mi esposa, ya que estabamos muy nerviosos y confundidos. Al aclararnos todas las dudas se nos facilito responder las preguntas del consul, es por esto que se nos ha confirmado que aprobamos para la visa americana, gracias por todo."</p>
						</div>
						<div class="client-info text-center">
						
						<!-- /.client name -->
							Jennifer Lopez, <span class="company">Bogota</span>	
						</div>
					</div>		
					
					<!-- /.testimonial 2 -->
					<div class="testi-item">
						<div class="client-pic text-center">

							<!-- /.client photo -->
							<!-- <img src="images/client.jpg" alt="client"> -->
						</div>
						<div class="box">

							<!-- /.testimonial content -->
							<p class="message text-center">"Hola mi niña, muchas gracias y te felicito por tu asesoria en este tramite para visa americana, ya que en realidad me informaste de todos los requisitos para la visa americana. Muchos exitos."</p>
						</div>
						<div class="client-info text-center">

							<!-- /.client name -->
							Gabriel Estrada, <span class="company">Santa Marta</span>	
						</div>
					</div>				
					
					<!-- /.testimonial 3 -->
					<div class="testi-item">
						<div class="client-pic text-center">

							<!-- /.client photo -->
							<!-- <img src="images/client.jpg" alt="client"> -->
						</div>
						<div class="box">

							<!-- /.testimonial content -->
							<p class="message text-center">"Hola Patricia, te quuiero contar que hoy me presente a la embajada americana y me aprobaron mi visa te doy gracias por la ayuda que me prestaste y estare en contacto para que me cotises los tiquetes a New York."</p>
						</div>
						<div class="client-info text-center">

							<!-- /.client name -->
							Liliana Aristizabal, <span class="company">Cali</span>	
						</div>
					</div>			
				 
				</div>
			</div>	
		</div>	
	</div>
</div>

<!-- /.contact section -->
<div id="contact">
	<div class="contact fullscreen parallax" style="background-image:url('images/bg.jpg');" data-img-width="2000" data-img-height="1334" data-diff="100">
		<div class="overlay">
			<div class="container">
				<div class="row contact-row">
				
					<!-- /.address and contact -->
					<div class="col-sm-5 contact-left wow fadeInUp">
						<h2><span class="highlight">Contactenos</span> Ahora</h2>
							<ul class="ul-address">
							<li><i class="pe-7s-map-marker"></i>Asesoria Vitual desde la ciudad de Bogota</br>
							</li>
							<li><i class="pe-7s-phone"></i>+57 320 419 9366</br>

							</li>
							<li><i class="pe-7s-mail"></i><a href="#">ventas@agenciadestinos.net</a></li>
							<li><i class="pe-7s-look"></i><a href="https://www.agenciadestinos.net">www.agenciadestinos.net</a></li>
							</ul>	
								
					</div>
					
					<!-- /.contact form -->
					<div class="col-sm-7 contact-right">
						<form method="POST" id="contact-form" class="form-horizontal" action="contactengine.php" onSubmit="alert('Thank you for your feedback!');">
							<div class="form-group">
							<input type="text" name="Name" id="Name" class="form-control wow fadeInUp" placeholder="Nombre" required/>
							</div>
							<div class="form-group">
							<input type="text" name="Email" id="Email" class="form-control wow fadeInUp" placeholder="Correo Electronico" required/>
							</div>					
							<div class="form-group">
							<textarea name="Message" rows="20" cols="20" id="Message" class="form-control input-message wow fadeInUp"  placeholder="Mensaje" required></textarea>
							</div>
							<div class="form-group">
							<input type="submit" name="submit" value="Submit" class="btn btn-success wow fadeInUp" />
							</div>
						</form>		
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
  
<!-- /.footer -->
<footer id="footer">
	<div class="container">
		<div class="col-sm-4 col-sm-offset-4">
			<!-- /.social links -->
				<div class="social text-center">
					<ul>
						<li><a class="wow fadeInUp" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
						<li><a class="wow fadeInUp" href="https://www.facebook.com/" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
						<li><a class="wow fadeInUp" href="https://plus.google.com/" data-wow-delay="0.4s"><i class="fa fa-google-plus"></i></a></li>
						<li><a class="wow fadeInUp" href="https://instagram.com/" data-wow-delay="0.6s"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>	
			<div class="text-center wow fadeInUp" style="font-size: 14px;">Agencia Destinos </div>
			<a href="#" class="scrollToTop"><i class="pe-7s-up-arrow pe-va"></i></a>
		</div>	
	</div>	
</footer>
	
	<!-- /.javascript files -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/jquery.sticky.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script>
		new WOW().init();
	</script>
  </body>
</html>