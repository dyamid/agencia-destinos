<?php 

$lang = array (
	"inicio" => "Home",
	"titulo" => "Agency destinations",
	"descripcion" => "Agency of Travels",
	"paqnac" => "National Packages",
	"ir" => "Go",
	"ir a" => "to package",
	"paqint" => "International Packages",
	"paqiba" => "Ibague Packages",
	"visame" => "American Visa",
	"alqfin" => "Fincas Rental",
	"vuehot" => "Flights and Hotels",
	"idioma" => "Language",
	"verballe" => "Whale watching",
	"sanandres" => "San Andres islands",
	"guajira" => "The Guajira",
	"providencia" => "Providence island tour",
	"margarita" => "Margarita island",
	"cancun" => "cancun",
	"disney" => "Disney World",
	"cajamarca" => "passadia in cajamarca town and Volcano hill Machin",
	"machine" => "Volcano hill Machin",
	"natural" => "Ibague Natural",
	"extremo" => "Extreme tourism",
	"ibaguemusical" => "Ibague Musical city",
	"ibaguenatural" => "Ibague city Natural, Combeima Canyon",
	"visa1" =>"DO YOU NEED TO PROCESS YOUR VISA?",
	"visa1.1" =>"We do it for you.
Fast, reliable and economical!   ",
	"visa2" =>"EXCELLENT RESULTS",
	"visa2.1" => "We guide all your process before the American embassy",
	"visa3" =>"WE ARE EXPERTS!",
	"visa3.1" =>"Contact us and leave your American Visa application",
	"polisos" => "Sustainability policy",
	"nosotros" => "We",
	"contac" => "Contac Us",
	"titumision" => "Mission",
	"tituvision" => "Vission",
	"mision" => "To offer quality tourist services by promoting nature tourism as a strategy to promote and maintain physical and mental health and the care of receptive tourism at national and international levels, being at the forefront of improving quality every day.",
	"vision" => "To be recognized in 2020 as an agency that promotes tourism as an activity necessary to promote and maintain physical and mental health, specializing in nature tourism and being national and international spokespeople in the care of receptive tourism.",
	"politica" => "At Agencia Destinos, we are committed to providing clients with a natural, experiential and recreational tourism that involves innovative activities that allow them to live unforgettable moments that mark tourism in their lives as an activity of growth and mental and emotional relaxation that will help us relax and Prevent diseases caused by daily living.<br />

		In a commitment to the development of our region we also focus on promoting and selling local destinations supporting the economy of the area's population. Promoting and emphasizing in respect for their culture their idiosyncrasy and historical and natural heritage, both tangible and intangible <br />

		Aware of the social decadence and the need for personal and business commitment to contribute to the improvement of the same we sensitize and publicize the campaigns against the ESCNA (sexual exploitation of children and adolescents) <br />

		Agencia Destinos are committed to minimizing the environmental impact by promoting an awareness of love, care and respect for the environment. Taking our clientele to be aware of respect, recycling and care for our nature.<br />


		All this in the middle of the articulation with providers of services committed with the security and the good attention of the tourists.<br />



		Approved on January 7, 2016 in Ibagué Tolima by Patricia Rojas Garcés Manager agency destinations.<br /><br /><br />





		Objectives of sustainability. <br /><br />

		1. Minimize energy consumption within our office. <br />
		2. Preserve and protect the flora of the agency.
		3. using recyclable material to minimize the impact on natural resources.
			4. Provide adequate management of solid and hazardous waste.
		5. Train and raise staff awareness in environmental, socio-cultural and economic standards, <br />
		6. Conduct collection and solid waste campaigns and recycling
		7. Promote the purchase of products made in the regions to visit, supporting the economy of each place. <br />
		8. Participate in activities and trainings that are carried out with the different communities.
		9. Make known the rules of prevention of the ESCNA, giving constant information on the laws and penalties against this scourge. If you know of a fact report it to the competent authority, work with the tourism police to encourage the application of the rule in other agencies and providers of tourism services.
		10. Provide the best human quality and detailed information in the services provided. <br />
		11. Support local communities and destinations to improve their quality of life.
		12. Comply with current legislation. <br /> ",
 "titulopaqueteavistamientodeballenas" =>"WHALE WATCH",
 "paqueteavistamientodeballenas" => "The plan includes: <br /> Transportation in tourism vehicles Ibagué - Buenaventura, Buenaventura - Ibague. <br /> Transfer by boat from Buenaventura - Hotel in Ladrilleros - Buenaventura. <br /> One night accommodation in Ladrilleros. /> Food Breakfast (2) Lunch (2) and dinner (1) <br /> Whale watching tour <br /> Companion guide <br /> Local guide <br /> Travel insurance < Medical Assistance Card. <br />
  Date: 22nd and 23rd of October. <br />
  Price: $ 390,000.
  <br /> These packages are subject to change depending on availability of airlines and hotels.",
"titupaqueteguajira" => "¡The Guajira a wonderful destination!",
 "paqueteguajira" =>"Duration: 3 days, 2 nights for $750.000. <br />It includes:. <br />
Overnight at hotel Waya Guajira. <br />Transfers  Riohacha Airport-Hotel Guaya- Riohacha Airport. <br />Breakfast buffet. <br />Lunch and dinner at the chef's choice. <br />Snack: two options in the day (snack with juice and soda). <br />Alcoholic and non-alcoholic drinks (according to hotel hours 10am to 11pm) guajira-tour-punta-hens-tour-cabo-de-la-vela-tour-santa marta-colombia-expotur-.<br />Wet areas (pool and jacuzzi). <br />Fruit Punch Welcome drink. <br />Ecological footpath (duration 1 hour). <br />Taxes. <br />Travel insurance. <br />Transfers and visits to:. <br />Mayapo beach or cape of the candle. <br />Rancheria Wayuu. <br />
Does not include: <br />Air ticket from the city of origin. <br />Gratuities, extras like laundry and others. <br />Expenditures not specified. <br />These packages are subject to availability of airlines and hotels. <br />",
 "tituprov" => "Isla Providencia",
 "paqueteprovidencia" => "<br /> The plan includes: <br />
Airfare Bogota - San Andrés island - Bogota <br />
San Andrés airfare - providencia - San Andrés <br />
Transfers airport - hotel - airport <br />
Hotel accommodation for the nights chosen <br />
Breakfast includedsan-andrc3a9s-sea-of-the-seven-colors <br />
Hotel taxes <br />
Travel insurance <br />
5 DAYS ALL INCLUSIVE PLAN FROM:
$ 2,195,000 <br />
These packages are subject to change depending on availability of airlines and hotels. <br />",
"titusanandres" => "Island Of San Andres",
"paquetesanandres" => "A wonderful tourist paradise to enjoy <br />
INCLUDES: foto-sanandres <br /> Air ticket Bogota-San Andres-Bogota <br /> Transfer airport-Hotel-Airport <br /> Accommodation: 4 nights 5 days <br />
Food: Breakfast and Dinner <br /> Jhonny Cay Aquarium <br /> Return to the Island and Rocky <br />
Bahia Tour <br /> Medical Assistance Card <br /> Travel Insurance <br />
Tourist guides <br /> OBSEQUIO <br /> Lunch at Jhinny Cay and Colorino <br /> Entrance to the swimming pool <br /> Morgan's cave <br /> FROM: $ 800,000 <br />
NOT INCLUDED: <br /> Entrance to unspecified tourist sites <br /> Drinks (ALCOHOLIC AND NON-ALCOHOLIC) <br /> Entrance ticket to the island ($ 99,000). <br />
These packages are subject to change depending on availability of airlines and hotels.",
"titudisney" => "Disney World",
"paquetedisney" => "Make a reality of all your dreams in four unique theme parks. <br /> 8 Day (s) - 7 Night (s) <br /> Transfers, Lodging 7 Nights, Park Tickets, Magic Hours Disney. <br /> FROM : 710 USD <br /> INCLUDES: <br /> Free shuttle service to Disney Magical Express Orlando Airport - Orlando Airport Hotel <br /> Accommodation 7 Nights 8 days in room Standard with taxes at a selected Disney Hotel /> 4 days admission to a theme park per day to choose from Disney's Magic Kingdom, Disney's Epcot, Disney's Animal Kingdom and Disney's Hollywood Studios. Pass offers Fastpass service and expires 14 days after first use. If you wish you can book your Fast Pass from Colombia 30 days before the start of your vacation by creating your Disney user account by downloading the My Disney Experience App on your cell phone or by visiting the mydisneyexperience page. A theme park will open its doors 1 hour before opening to the general public or close 3 hours later than usual hours. <br /> Additional parking. <br /> Check in: 4.00 pm. Check out: 11.00 am <br /> Right to use all Walt Disney World transportation system: Buses, Monorail and Ferry depending on the location of the hotel. <br /> <br /> NOT INCLUDED: <br /> Air ticket , Taxes and costs. Disney characters perform in front of the Cinderella Castle, Magic Kingdom, Walt Disney World, Orlando, Florida USA <br /> Transfers unspecified <br /> Other unspecified expenses <br /> Food. <br />
Air fares and taxes taxes or contributions that tax them. Such as: VAT, tax, fuel tax, administrative fee, airport taxes and departure from the countries of origin and destination, other charges (subject to change). (Consult with your advisor for documentation requirements and preventive health measures for the main destination.) <br /> Check-in at hotels before the scheduled time 3:00 p.m. and departure after 12:00 m. (Hours subject to change without prior notice). <br /> Personal expenses and additional services such as: room service, telephone service, laundry, etc. <br /> Any expenses or service not detailed. <br /> < These packages are subject to change depending on availability of airlines and hotels. <br />",
"titucancun" => "Cancun-A wonderful tourist paradise to enjoy.",
"paquetecancun" => " <br /> The plan includes: <br />
Air ticket from Bogotá <br /> Transfer Airport / Hotel / Airport <br /> Holiday Inn Cancun Arenas <br /> Accommodation in standard double room <br /> 3 or 5 or 7 nights accommodation <br /> Plan all Included in the hotel Breakfast lunch and cenacancun <br /> Alcoholic and non-alcoholic drinks Unlimited and Snacks <br /> Entertainment <br /> Non-motorized water sports <br /> Gym <br /> Standard room base <br /> Hotel taxes <br /> Holiday card-assistance-inn-arenas-pool <br /> <br /> Not included: <br /> Tips, extras like laundry and others <br /> Expenses not specified. >",
"titumargarita" => "Margarita Islands-Meet the most beautiful beach in Venezuela",
"paquetemargarita" => "The plan includes: Air Ticket Bogotá or Medellin / Isla Margarita / Bogotá or Medellín via AVIOR <br /> Transfer Airport / Hotel / Airport. <br /> 3, 4, 5, 6 or 7 Nights in Isla Margarita With ALL INCLUSIVE <br /> Unlimited Alcoholic and Non-alcoholic Beverages and Snacks. <br /> Daily Activities and Night Show <br /> Island-margarita beaches <br /> Non-motorized Water Sports <br /> Standard Room Base <br/> < > Hotel Taxes <br /> These packages are subject to change depending on availability of airlines and hotels. <br />",
"titumachine" => "Pasadia en Cajamarca y volcan cerro Machin",
"paquetecajamarca" => "Live a beautiful experience together with the thermal springs of the volcano Machin hill <br /> Check out Saturdays and Sundays <br /> INCLUDES: <br /> Transport Departure Ibagué-Cajamarca in Buseta <br /> Transportation Cajamarca Cerro Machin in Campero <br /> < > Breakfast in the Corregimiento de Toche <br /> Trek through Cerro Machín (2 Ecological Trails 'According to physical state') <br /> Peasant Lunch <br /> <br /> Bath in Hot Springs <br /> Travel Insurance <br /> Companion Guide <br /> VALUE: 105,000 <br /> These packages are subject to change depending on availability of airlines and hotels. <br />",
"tituextremo" => "Turismo Extremo",
"paqueteextremo" => "Live Ibague to the End <br /> EXTREME TOURING: 1 day <br />
Departure 7:00 am <br /> Place Plaza Bolivar <br /> FROM: $ 115,000 <br /> Displacement to the path where we will start the walk <br /> Arriving at the waterfalls Torrentismo or canyonig activity for those interested. > Duration hour and a half. <br /> From there we head to the Finca where we will have lunch. <br /> After lunch we move to Mirador Buena vista, to see beautiful landscape in palaces and some lagoons. <br /> There will be a time of Yincanas games with the objective of integrating the group. <br /> Departure to the city of Ibagué about 4:30. <br /> INCLUDES: <br /> Transportation from Ibague < 1 refreshments <br /> 1 refreshments <br /> 1 lunches <br /> Extreme sport guide <br /> Recreating guide <br /> Medical card for extreme sports <br /> These Packages are subject to change depending on availability of airlines and hotels. <br />",
"tituibague" => "Ibague Ciudad Musical",
"paqueteibague" => "Ciudad Musical, <br /> Duration Two days 1 night. <br /> <br /> Day 1: cathedral-ibague <br /> Cheking and welcome. <br /> Cultural Heritage Route in Chiva by the city of Ibagué. <br /> Visit to the Orquidiario del Tolima <br /> Visit to the hall Alberto Castillo <br /> Day of integration with folkloric shows <br /> Day 2. <br /> < <br /> Enjoy the farm where you will stay. <br /> Includes: <br /> Accommodation in tourist farm. <br /> 1 breakfasts, 2 lunches, 1 dinner, 2 snacks. > Tour Guide in the Canyon <br /> Souvenir. <br /> Travel Insurance <br /> * These packages are subject to change depending on availability of airlines and hotels. <br />",
"paqueteibaguenatural" => "Enjoy the Combeima Canyon <br /> Duration: Two days and one night <br /> Day 1: <br /> Cheking and welcome <br /> Extreme sport canyoning with a travel agency specializing in adventure tourism in the sector <br /> Day 2: images <br /> Visit La Finca Estate, includes: <br /> Lunch, return and return by cable car <br /> , Path Ecological and reforestation <br /> Chiva tour by Ibagué for 'the traces of Count Gabriac' <br /> Includes: <br /> Lodging one night hotel in the Conbeima Canyon <br /> Two breakfasts, two lunches, two Dinners and three snacks <br /> Tourist guide in the Canyon <br /> Suvenir. <br /> Travel insurance <br /> * These packages are subject to change depending on availability of airlines and hotels. <br />",
"cana" => "Canadian Visa"



	);

 ?>