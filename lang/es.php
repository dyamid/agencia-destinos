<?php 

$lang = array (
	"inicio" => "Inicio",
	"titulo" => "Agencia Destino",
	"descripcion" => "agencia de viajes",
	"paqnac" => "Paquetes Nacionales",
	"ir" => "Ir",
	"ir a" => "al Paquete",
	"paqint" => "Paquetes Internacionales",
	"paqiba" => "Paquetes Ibague",
	"visame" => "Visa Americana",
	"alqfin" => "Alquiler Fincas",
	"vuehot" => "Vuelos y Hoteles",
	"idioma" => "idioma",
	"verballe" => "Avistamiento De Ballenas",
	"sanandres" => "San Andres islas",
	"guajira" => "La Guajira",
	"providencia" => "Tour Isla Providencia",
	"margarita" => "Isla Margarita",
	"cancun" => "cancun",
	"disney" => "Disney World",
	"cajamarca" => "pasadia en cajamarca y volcan cerro machin.",
	"machine" => "Volcan Machin",
	"extremo" => "turismo extremo",
	"ibaguemusical" => "Ibague Ciudad Musical",
	"natural" => "Ibague Natural",
	"ibaguenatural" => "Ibague Natural, Cañon del combeima",
	"visa1" =>"NECESITA	TRAMITAR SU VISA",
	"visa1.1" =>"Nosotros lo hacemos por usted.
	 Rapido, confiable y economico!   ",
	"visa2" =>"EXCELENTE RESULTADOS",
	"visa2.1" => "Le guiamos todo su tramite ante la embaja Americana",
	"visa3" =>"SOMOS EXPERTOS!",
	"visa3.1" =>"Contactenos y deje su solicitud de Visa Americana ",
	"polisos" => "Politica De Sosteniblidad",
	"nosotros" => "Nosotros",
	"contac" => "Contactenos",
	"titumision" => "Mision",
	"tituvision" => "Vision",
	"vision" => "Ser reconocidos en el 2020 como una agencia que fomenta el turismo como una actividad necesaria para promover y mantener una salud física y mental, especializándonos en turismo de naturaleza y siendo voceros a nivel nacional e internacional en la atención del turismo receptivo.",
	"mision" => "Ofertar servicios turísticos calidad mediante la promoción del turismo de naturaleza como estrategia para promover y mantener la salud física y mental, y la atención del turismo receptivo a nivel nacional e internacional estando a la vanguardia cada dia en mejoramiento de la calidad.",
	"politica" => 	"En Agencia Destinos estamos comprometidos en ofrecer al cliente turismo de naturaleza , experiencial y recreativo que conlleva actividades innovadoras que le permitan vivir momentos inolvidables, que marquen en sus vidas el turismo como una actividad de crecimiento y esparcimiento mental y emocional que nos ayude a relajar y prevenir enfermedades causadas por el diario vivir.<br />
		En un compromiso con el desarrollo d nuestra región nos enfocamos también en promocionar y vender destinos  locales apoyando la economía de la población del área de influencia. Promocionando y enfatizando en el respeto por  su cultura sus idiosincrasia y patrimonio histórico y natural, tanto tangible como intangible <br />

		Conscientes de la decadencia social y la necesidad de compromiso personal y empresarial  en aportar para el  mejoramiento de la misma  sensibilizamos y damos a conocer las campañas contra  el ESCNA (explotación sexual de niños niñas y adolescentes) <br />

		Agencia destinos está comprometida a minimizar el impacto ambiental promoviendo una conciencia de 	amor, cuidado y respeto por el medio ambiente. Llevando a nuestra clientela a que se concientice del cuidado que debemos mantener con la naturaleza.<br />

		Todo esto en medio de una articulación de prestadores de servicios comprometidos con la seguridad y la buena atención de los turistas.<br />


		Aprobada el día 7 de enero del 2016 en Ibagué Tolima por Patricia Rojas Garcés Gerente agencia destinos.<br /><br /><br />





			Objetivos de sostenibilidad. <br /><br />

		1.	Minimizar consumo de energía dentro de nuestra oficina.<br />
		2.	Conservar y proteger la flora de la agencia.<br />
		3.	 utilizando material reciclable para minimizar el impacto a los recurso naturales.
		4.	Brindar el manejo adecuado a los residuos sólidos y peligrosos.<br />
		5.	Capacitar y concientizar el personal en normas ambientales, socioculturals y económicos,<br /> 
		6.	Realizar campañas de recolección y de residuos sólidos y reciclaje<br />
		7.	Promover la compra de productos elaborados en las regiones a visitar, apoyando la economía de 		cada lugar.<br />
		8.	Participar de actividades y capacitaciones que se realicen con las diferentes comunidades.<br />
		9.	Dar a conocer las normas de prevención del ESCNA, dando información constante sobre las leyes y 		penas contra dicho flagelo. En caso de conocer algún hecho reportarlo ante la autoridad 		competente, trabajar de la mano de la policía de turismo para fomentar la aplicación de la norma 		en otras agencias y prestadores de servicios turísticos.<br />
		10.	Brindar la mejor calidad humana e información en detallada en los servicios prestados.<br />
		11.	Apoyar la las comunidades locales y en los destinos para mejorar la calidad de vida de las mismas
		12.	Cumplir la legislación legal vigente.<br />",


"titulopaqueteavistamientodeballenas" =>"AVISTAMIENTO DE BALLENAS EN CHOCO",
 "paqueteavistamientodeballenas" => "El plan incluye:<br /> Transporte en vehículos de turismo  Ibagué — Buenaventura, Buenaventura —Ibagué.<br /> Traslado en lancha desde Buenaventura — Hotel en Ladrilleros — Buenaventura.<br /> Una noche de alojamiento en Ladrilleros.<br /> Alimentación Desayuno (2) Almuerzo (2) y cena (1).<br /> Tour de avistamiento de ballenas.<br /> Guía acompañante.<br /> Guía local.<br /> Seguro de viaje.<br /> Tarjeta de asistencia medica.<br /> 
 	Fecha:22 y 23 de Octubre.<br />
 	Precio: $390.000.
 	<br /> Estos paquetes están sujetos a cambios según disponibilidad de aereolineas y hoteles.",
 "titupaqueteguajira" => "¡La Guajira un destino maravilloso!",
 "paqueteguajira" =>"Duración: 3 dias, 2 noches desde 750.000.<br />Incluye: .<br />	
		Alojamiento en el hotel Waya Guajira.<br />	Traslados de llegada y salida Aeropuerto Riohacha-hotel guaya- Aeropuerto Riohacha.<br />Desayuno Buffet.<br />Almuezos y cenas a elección del chef.<br />Snack: dos opciones en el día (pasaboca con jugo y gaseosa).<br />Bebidas alcohólicas y no alcohólicas (según horarios del hotel 10am a 11pm)		guajira-tour-punta-gallinas-tour-cabo-de-la-vela-tour-santa-marta-colombia-expotur-.<br />Zonas Húmedas (piscina y jacuzzi).<br />Fruit punch Bebida de bienvenida.<br />		
		Sendero ecológico (duración 1 hora).<br />Impuestos.<br />Seguro de viaje.<br />Traslados y visitas a:<br />Playa Mayapo o cabo de la vela.<br />Ranchería Wayuu.<br />No incluye:.<br />Tiquete aéreo desde la ciudad de origen.<br />	Propinas, extras como lavandería y otros.<br />Gastos no especificados.<br />Estos paquetes están sujetos a cambios según disponibilidad de aereolineas y hoteles.<br />",
"tituprov" => "Isla Providencia",
 "paqueteprovidencia" => "<br />El plan incluye:<br />
Tiquetes aéreos Bogotá – San Andrés isla – Bogotá<br />
Tiquetes aéreos San Andrés – providencia – San Andrés<br />
Traslados aeropuerto – hotel – aeropuerto<br />
Hospedaje en hotel por las noches elegidas<br />
Desayunos incluidossan-andrc3a9s-mar-de-los-siete-colores<br />
Impuestos hoteleros<br />
Seguro de viajes<br />
PLAN 5 DIAS TODO INCLUIDO DESDE:
$2.195.000<br />
Estos paquetes están sujetos a cambios según disponibilidad de aereolineas y hoteles.<br />",
"titusanandres" => "Isla De San Andres",
"paquetesanandres" => "Un maravilloso paraiso turistico para disfrutar<br />
INCLUYE:foto-sanandres<br />Tiquete aereo Bogota-San Andres-Bogota<br />Traslado aeropuerto-Hotel-Aeropuerto<br />Alojamiento:4 noches 5 dias<br />
Alimentacion:Desayuno y Cena<br />Jhonny Cay Acuario<br />Vuelta a la Isla y Rocky<br />
Tour Bahia<br />Tarjeta de asistencia medica<br />Seguro de viajes<br />
Guias turisticos<br />OBSEQUIO<br />Almuerzo en Jhinny Cay e impuesto Colorino<br />Entrada piscinitacabeza-morgan<br />Cueva de Morgan<br />DESDE: $800.000<br />
NO INCLUYE:<br />Entrada a sitios turisticos no especificados<br />Bebidas (ALCOHOLICAS Y NO ALCOHOLICAS)<br />Tarjeta entrada a la isla ($ 99.000).<br />

Estos paquetes están sujetos a cambios según disponibilidad de aereolineas y hoteles.",
"titudisney" => "Disney World",
"paquetedisney" => "Haz una realidad de todos tus sueños en cuatro parques temáticos singulares.<br />8 Día(s) –  7Noche(s)<br />Traslados, Alojamiento 7 Noches, Entradas a parques, Horas magicas Disney.<br />DESDE: 710 USD<br />INCLUYE:<br />Traslado gratuito en servicio Disney Magical Express aeropuerto Orlando – Hotel    – aeropuerto Orlando<br />Alojamiento 7 Noches 8 dias en habitación Estandar con impuestos en un Hotel Disney´s elegido<br />4 días de admisión a un parque temático por día a escoger entre Disney´s Magic Kingdom, Disney´s Epcot, Disney´s Animal Kingdom y Disney´s Hollywood Studios. Pase ofrece servicio Fastpass y expira 14 días después de su primer uso. Si lo desea puede reservar sus Fast Pass desde Colombia 30 días antes del inicio de sus vacaciones creando su cuenta de usuario Disney, descargando el App My Disney Experience en su celular o visitando la página mydisneyexperience.<br />Horas Mágicas Disney: Cada día un parque temáti00000000000co abrirá sus puertas 1 hora antes de la apertura al público en general o cerrara hasta 3 horas más tarde de su horario habitual.<br />Estacionamiento complementario.<br />Check in: 4.00 pm. Check out: 11.00 am<br />Derecho a utilizar Gratistodo el sistema de transporte de Walt Disney World: Buses, Monorriel y Ferry dependiendo la ubicación del hotel.<br /><br />NO INCLUYE:<br />Boleto aéreo, impuestos y sobre costos.<br />Disney characters perform in front of the Cinderella Castle, Magic Kingdom, Walt Disney World, Orlando, Florida USA<br />Traslados no especificados<br />Otros gastos no especificados<br />Alimentación.<br />
Pasajes aéreos e impuestos tasas o contribuciones que los graven. tales como: Iva, tasa, impuestos de combustible, tarifa administrativa, impuestos de aeropuertos y salida de los países de origen y destino, otros cargos (sujetos a cambio).<br />Tramite de visas. (Consulte con su asesor los requisitos de documentación y medidas de salud preventiva para destacada_disneyel destino).<br />Registro de entrada en hoteles antes de la hora prevista 15:00 y de salida después de las 12:00 m. (Horarios sujetos a cambio sin previo aviso).<br />Gastos personales y servicios adicionales como: servicio a la habitación, servicio telefónico, lavandería, entre otros.<br />Cualquier gasto o servicio no detallado.<br /><br />Estos paquetes están sujetos a cambios según disponibilidad de aereolineas y hoteles.<br />",
"titucancun" => "Cancun-Un maravilloso paraiso turistico para disfrutar.",
"paquetecancun" => "<br />El  plan incluye:<br />
Tiquete Aéreo desde Bogotá<br />Traslado Aeropuerto/ Hotel/ Aeropuerto<br />Hotel Holiday Inn Cancún Arenas<br />Alojamiento en habitación doble estándar<br />3 o 5 o 7 noches de alojamiento<br />Plan todo incluido en el hotel Desayuno almuerzo y cenacancun<br />Bebidas Alcohólicas y No alcohólicas Ilimitadas y Snacks<br />Entretenimiento<br />Deportes Acuáticos No Motorizados<br />Gimnasio<br />Base en habitación estándar<br />Impuestos hoteleros<br />Tarjeta de asistenciacancun-holiday-inn-arenas-piscina<br /><br />No Incluye:<br />Propinas, extras como lavandería y otros<br />Gastos no especificados.<br />",
"titumargarita" => "Islas Margarita-Conoce la playa más hermosa de Venezuela",
"paquetemargarita" => "<br />Desde el 21 de Diciembre al 06 de Enero del 2017
El plan incluye:<br />Tiquete Aéreo Bogotá o Medellín / Isla Margarita/ Bogotá o Medellín vía AVIOR<br />Traslado Aeropuerto/ Hotel/ Aeropuerto.<br />3, 4, 5, 6 o 7 Noches en Isla Margarita con TODO INCLUIDO<br />Bebidas Alcohólicas y No alcohólicas Ilimitadas y Snacks.<br />Actividades Diarias y Show Nocturnosplayas-de-isla-margarita<br />Deportes Acuáticos No Motorizados<br />Base en habitación estándar<br />Impuestos hoteleros<br />Estos paquetes están sujetos a cambios según disponibilidad de aereolineas y hoteles.<br />",
"titumachine" => "Pasadia en Cajamarca y volcan cerro Machin",
"paquetecajamarca" => "<br /> Compruebe los sábados y domingos <br /> INCLUYE: <br /> Transporte Salida Ibagué-Cajamarca en Buseta <br /> Transporte Cajamarca Cerro Machin en Campero <br /> <> Desayuno en el Corregimiento de Toche <br /> Caminata a través de Cerro Machín (2 senderos ecológicos según el estado físico) <br /> <br /> Almuerzo campesino <br /> <br /> Baño en aguas termales <Guía de Compañía de Seguros de Viaje VALOR: 105,000 Estos paquetes son Sujeto a cambios dependiendo de la disponibilidad de aerolíneas y hoteles. <br />",
"tituextremo" => "Turismo Extremo",
"paqueteextremo" => "Vive Ibague al Extremo<br />TURISMO EXTREMODuración: 1 día<br />
Salida 7:00am<br />Lugar Plaza Bolívar<br />DESDE: $115.000<br />Desplazamiento  hasta la vereda donde iniciaremos la caminata<br />Llegando a las cascadas actividad de Torrentismo o canyonig para los interesados.<br />Duración hora y media.<br />De ahí nos dirigimos caminando a la Finca donde tomaremos el almuerzo.<br />Después del almuerzo nos desplazamos hasta el Mirador Buena vista, para divisar hermoso paisaje en gamas de colores y algunas lagunas.<br />Allí se realizara un tiempo de juegos Yincanas con el objetivo De integrar al grupo.<br />Salida hacia la ciudad de Ibagué sobre las 4:30.<br />INCLUYE:<br />Transporte desde Ibague<br />1 refrigerio<br />Actividades de torrentismo y recreación<br />1 almuerzos<br />Guía de deporte extremo<br />Guía recreador<br />Tarjeta de asistencia médica para deporte extremo<br />Estos paquetes están sujetos a cambios según disponibilidad de aereolineas y hoteles.<br />",
"paqueteibague" => "Ciudad Musical,<br />Duración Dos días 1 noche.<br /><br />Día 1:catedral-ibague<br />Cheking y bienvenida.<br />Recorrido Patrimonio cultural en chiva por la ciudad de Ibagué. Conociendo los lugares iconos de la ciudad.<br />Visita al Orquidiario del Tolima<br />Visita a la sala Alberto Castillo<br />Jornada de integración con muestra folclórica,<br />Día 2.<br /><br />Pajariada<br /> Disfrute de la finca donde se hospedaran.<br />Incluye:<br />Hospedaje en finca turística.<br />1 desayunos, 2 almuerzos, 1 cena, 2 refrigerios.<br />Guía turística en el Cañón<br />Recuerdo.<br />Seguro de viaje<br />*Estos paquetes están sujetos a cambios según disponibilidad de aereolineas y hoteles.<br />",
"paqueteibaguenatural" => "Disfruta el Cañon del Combeima<br />Duracion: Dos dias y una noche<br />Dia 1:<br />Cheking y bienvenida<br />Deporte extremo canyoning con una Agencia de viajes especializada en turismo de aventura en el sector<br />Reconocimiento cañon del combeima (cabalgata)<br />Jornada de integracióncon muestra folclórica<br />Dia 2:images<br /> Visita finca la Rivera, incluye:<br />almuerzo, ida y regreso en teleférico, sendero ecológico y reforestación<br />Recorrido en chiva por Ibagué por “las huellas del conde Gabriac”<br />Incluye:<br />Hospedaje una noche hotel en el Cañón del Conbeima<br />Dos desayunos, dos almuerzos, dos cenas y tres refrigerios<br />Guía turística en el Cañón<br />Suvenir.<br />Seguro de viaje<br />*Estos paquetes están sujetos a cambios según disponibilidad de aereolineas y hoteles.<br />",
"cana" => "Visa Canadiense"




	);

?>