<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Visados</title>

		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>
  <body>
    
    <div class="page">
    <?php require __DIR__.'/haeder.php'; ?>

      <section class="section-30 section-md-40 section-lg-66 section-xl-bottom-90 bg-gray-dark page-title-wrap" style="background-image: url(<?php echo $base_url ?>asesorias/images/imagenes/8.jpeg);">
        <div class="container">
          <div class="page-title">
            <h2>Visados</h2>
          </div>
        </div>
      </section>
      <section class="section-50 section-md-75 section-lg-100">
        <div class="container">
          <div class="row row-40">
            <div class="col-md-6 col-lg-4 height-fill">
              <article class="icon-box">
                <div class="box-top">
                  <div class="box-icon"><span class="novi-icon icon icon-primary icon-lg mercury-icon-briefcase"></span></div>
                  <div class="box-header">
                    <h5><a href="https://agenciadestinos.net/visa-reinounido/">Visa Reino Unido</a></h5>
                  </div>
                </div>
                <div class="divider bg-accent"></div>
                <div class="box-body">
                  <p>Asesoria profesional para realizar el tramite a Reino Unido.</p>
                </div>
              </article>
            </div>
            <div class="col-md-6 col-lg-4 height-fill">
              <article class="icon-box">
                <div class="box-top">
                  <div class="box-icon"><span class="novi-icon icon icon-primary icon-lg mercury-icon-users"></span></div>
                  <div class="box-header">
                    <h5><a href="https://agenciadestinos.net/visaca/inicio.php">Visa Canada</a></h5>
                  </div>
                </div>
                <div class="divider bg-accent"></div>
                <div class="box-body">
                  <p>Consultores profesionales para solicitar su visa Canadiense.</p>
                </div>
              </article>
            </div>
            <div class="col-md-6 col-lg-4 height-fill">
              <article class="icon-box">
                <div class="box-top">
                  <div class="box-icon"><span class="novi-icon icon icon-primary icon-lg mercury-icon-lib"></span></div>
                  <div class="box-header">
                    <h5><a href="https://agenciadestinos.net/asesoria-visa-americana/">Visa Americana</a></h5>
                  </div>
                </div>
                <div class="divider bg-accent"></div>
                <div class="box-body">
                  <p>Que tipo de visa necesito para viajar a estados unidos? esto y mas...</p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>

      <section class="section-35 section-md-top-75 section-md-bottom-50">
        <div class="container">
          <div class="col-md-12 row visados_box"  style="">
                <div class="col-md-6">
                  <h2 style="" class="title_visas">Visa Canadiense</h2>
                  <div class="inset-lg-right-40 inset-xl-right-70">
                    <p>Te asesoramos a escoger que tipo de visa te es elegible para viajar a canada. asesoria profesional en todo el proceso y servicio personalizado. diligenciamiento de formularios de manera completa y eficas para incremetar posibilidades. </p>
                    <p>si desea visa canadiense para su familia realizamos el tramite para cada solicitante de la familia, aprovechando un descuento especial indicandole todos los requisitos para su visa ante la embajada Canadiense.</p>
                    <a href="<?php echo $base_url ?>visaca/inicio.php">
                    <button class="btn btn-info">Ver mas informacion de visa Canadiense</button> 
                    </a>
                  </div>
                  
                </div>
                <div class="col-md-6">
                   <img src="<?php echo $base_url ?>asesorias/images/imagenes/2-canada.jpeg" width="100%">
                </div>
                <div class="col-md-12">
                    <h4>Preguntas</h4>
                    <?php require __DIR__.'/preguntas/visa-canada.php'; ?>
                  
                </div>
              </div>
             <div class="col-md-12 row visados_box"  style="">
                  <div class="col-md-6">
                    
                    <h2 style="" class="title_visas">Visa Reino Unido</h2>
                    <div class="inset-lg-right-40 inset-xl-right-70">
                      <p>La visa de turismo o visitante estándar se otorga a las personas que desean viajar al reino unido para el ocio, como vacaciones o turismo y para ver a su familia y amigos. </p>
                      <p>Nuestra empresa cuenta con un número de asesores, los cuales están capacitados para realizar una asesoría completa y eficaz. Estudio de documentacion para saber si es elegible o no. Diligenciamiento de formularios online o con un asesor. Asesoría para la documentación. Asesoría para las fotos. Asesoria para la toma de datos biometricos. Recibo de pago para sus derechos consulares.</p>
                      <a href="<?php echo $base_url ?>visa-reinounido/">
                      <button class="btn btn-info">Ver mas informacion de visa Reino Unido</button> 
                      </a>
                    </div>
                  </div>
                    <div class="col-md-6">
                     <img src="<?php echo $base_url ?>asesorias/images/imagenes/8.jpeg" width="100%">
                    </div>
                    <div>
                      <h4>Preguntas</h4>
                      <?php require __DIR__.'/preguntas/visa-reino-unido-preguntas.php'; ?>
                      
                    </div>
                </div>

            <div class="col-md-12 row visados_box"  style="">
              <div class="col-md-6">
                <h2 style="" class="title_visas">Visa Americana</h2>
                
                <div class="inset-lg-right-40 inset-xl-right-70">
                  <p>Realizamos el trámite de visa ante la pagina del AIS para inscripción, pago y solicitud de citas para huellas y entrevista de su solicitud de visa americama ante la embajada. </p>
                  <p>Le brindamos su itinerario de viaje y vuelos para la solicitud de su visa americana, todo sin costo adicional, además cotizamos su viaje y le damos los mejores precios y ofertas.</p>
                  <a href="<?php echo $base_url ?>asesoria-visa-americana/">
                  <button class="btn btn-info">Ver mas informacion de visa Americana</button> 
                  </a>
                </div>
                </div>
                   <div class="col-md-6">
                   <img src="<?php echo $base_url ?>asesorias/images/imagenes/3-uk.jpeg" width="100%">
                   </div>
              </div>
          </div>

      </section>
 <?php require __DIR__.'/footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>