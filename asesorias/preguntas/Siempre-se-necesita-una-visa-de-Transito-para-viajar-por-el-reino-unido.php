<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Que es la Visa de estudiante de corta Duración</title>
    
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo $base_url ?>asesorias/images/imagenes/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="page">
     <?php require __DIR__.'/../haeder.php'; ?>

<section class="section-40 section-md-bottom-100 section-xl-bottom-165">
        <div class="container">
          <h3> Siempre se necesita una visa de Transito para viajar por el reino unido?</h3>
          <div class="row row-30 justify-content-lg-between">
            <div class="col-md-6">
              <figure><img src="<?php echo $base_url ?>asesorias/images/imagenes/5-uk.jpg" alt="" width="570" height="386"/>
              </figure>
            </div>
            <div class="col-md-6">
              <div class="inset-lg-left-40 inset-xl-left-70 text-secondary">
              <p>Las personas de nacionalidad Colombiana están exentas de visa de tránsito si cumplen con las siguientes condiciones:</p>
              <p>
              El tránsito lo hacen por los aeropuertos de Gatwick LGW, Heathrow LHR o Manchester MAN
              <br>  Tienen un tiquete para un vuelo a un tercer país en el mismo día calendario y con los documentos necesarios para ingresar al otro país.
              <br>  Toda la ruta debe estar en un mismo tiquete
              <br>  No pueden cambiar de aeropuerto.
              <br>  No puede abandonar el área de tránsito
              <br>  Las maletas deben estar chequeadas hasta el destino final.
              <br>  Si la conexión es con una aerolínea de bajo costo deben confirmar si requiere la visa.

              </p>
             <li> <strong>Si el destino final es Irlanda del Sur Deben tramitar visa: </strong>Es impórtate tener en cuenta que si se han presentado datos biométricos y/o su solicitud ha sido procesada, no tendrá derecho a reembolsos ni de UK Visas and Inmigración ni del emisor de su tarjeta. Si realizó la solicitud para la categoría de visa equivocada, tendrá que retirar su solicitud y volver a realizarla.  </li>
         
              </div>
            </div>
          <h4>Preguntas</h4>
            <?php require __DIR__.'/visa-reino-unido-preguntas.php'; ?>
          </div>
        </div>
</section>

<?php require __DIR__.'/../footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="<?php echo $base_url ?>asesorias/js/core.min.js"></script>
    <script src="<?php echo $base_url ?>asesorias/js/script.js"></script>
  </body>
</html>