<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>¿Cuáles son los Requisitos para la visa de estudio?</title>
    
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo $base_url ?>asesorias/images/imagenes/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="page">
     <?php require __DIR__.'/../haeder.php'; ?>
<section class="section-40 section-md-bottom-100 section-xl-bottom-165">
        <div class="container">
          <h3>¿Cuáles son los Requisitos para la visa de estudio?</h3>
          <div class="row row-30 justify-content-lg-between">
            <div class="col-md-6">
              <figure><img src="<?php echo $base_url ?>asesorias/images/imagenes/3-canada.jpeg" alt="" width="570" height="386"/>
              </figure>
            </div>
            <div class="col-md-6">
              <div class="inset-lg-left-40 inset-xl-left-70 text-secondary">
                <p>Es impórtate que tenga claro cuál es la visa necesaria antes de aplicar, por lo tanto es necesario tener en cuenta:</p>
                <ul>
                	<li> presentar el Pasaporte vigente. Si se viaja con la familia, cada familiar deberá tener su pasaporte al día. </li>
                 <li> Presentar argumentos de que se regresará al país de origen luego de terminar los estudios</li>
                  <li> Gozar de buena salud y estar dispuesto a demostrarlo, sometiéndose a un examen médico.</li>
                  <li> Presentar pruebas de que cuenta con el dinero necesario para pagar sus estudios y su manutención, incluyendo la de su familia, si fuese el caso.</li>
                  <li> Presentar dos fotografías recientes (tamaño pasaporte) del solicitante y de cada miembro de la familia que lo acompañe. El nombre y la fecha de nacimiento de la persona deben ser escritos en el reverso de cada foto. 
                  </li>
                  <li> Probar que no se tienen antecedentes penales y que no se constituye un riesgo para la seguridad de Canadá.</li>
                  <li> Demostrar que se ha recibido la aceptación por parte de una escuela, colegio universitario o universidad en Canadá, presentando la respectiva carta de aceptación</li>

                </ul>
                
            </div>
          </div>
           <h4>Preguntas</h4>
            <?php require __DIR__.'/visa-canada.php'; ?>
          </div>
        </div>
</section>

<?php require __DIR__.'/../footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="<?php echo $base_url ?>asesorias/js/core.min.js"></script>
    <script src="<?php echo $base_url ?>asesorias/js/script.js"></script>
  </body>
</html>
       