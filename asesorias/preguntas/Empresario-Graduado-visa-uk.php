<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Empresario Graduado</title>
    
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo $base_url ?>asesorias/images/imagenes/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="page">
     <?php require __DIR__.'/../haeder.php'; ?>

<section class="section-40 section-md-bottom-100 section-xl-bottom-165">
        <div class="container">
          <h3>  Empresario Graduado</h3>
          <div class="row row-30 justify-content-lg-between">
            <div class="col-md-6">
              <figure><img src="<?php echo $base_url ?>asesorias/images/imagenes/3-uk.jpeg" alt="" width="570" height="386"/>
              </figure>
            </div>
            <div class="col-md-6">
              <div class="inset-lg-left-40 inset-xl-left-70 text-secondary">
              <p>Puede solicitar una visa de Nivel 1 (Graduate Entrepreneur) si: es un graduado que ha sido avalado oficialmente por tener una idea comercial genuina y creíble; son de fuera del Espacio Económico Europeo (EEE) y Suiza; cumplir con los otros criterios de elegibilidad</p>
             <li> <strong>Cuánto tiempo se demora? </strong>Por lo general se demora dependiendo quien lo respalda. </li>
            <li> <strong>Cuánto cuesta: </strong> 349 Euros</li>
            <li><strong>Cuánto tiempo puede permanecer: </strong>Puede permanecer durante un año</li>
            <li> <strong>Que puede hacer?: </strong>Traer miembros de la familia contigo; extender su estadía en el Reino Unido utilizando esta visa; cambiar a esta visa de otras categorías de visa.
            </li>
            <li> <strong>No puedes: </strong>Obtener fondos públicos; trabajar como un doctor o dentista en entrenamiento; trabajar como deportista profesional, por ejemplo, un entrenador deportivo; establecerse en el Reino Unido con esta visa </li>
         
              </div>
            </div>

          <h4>Preguntas</h4>
            <?php require __DIR__.'/visa-reino-unido-preguntas.php'; ?>
          </div>
        </div>
</section>

<?php require __DIR__.'/../footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="<?php echo $base_url ?>asesorias/js/core.min.js"></script>
    <script src="<?php echo $base_url ?>asesorias/js/script.js"></script>
  </body>
</html>