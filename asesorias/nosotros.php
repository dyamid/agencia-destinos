<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>About Us</title>

		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>
  <body>
    
    <div class="page">
      
<?php require __DIR__.'/haeder.php'; ?>
      <section class="section-30 section-md-40 section-lg-66 section-xl-bottom-90 bg-gray-dark page-title-wrap" style="background-image: url(<?php echo $base_url ?>asesorias/images/imagenes/6-canada.jpeg);">
        <div class="container">
          <div class="page-title">
            <h2>Nosotros</h2>
          </div>
        </div>
      </section>

      <section class="section-66 section-md-90 section-xl-bottom-120">
        <div class="container">
          <h3>Quienes somos?</h3>
          <div class="row row-40 justify-content-xl-between align-items-lg-center">
            <div class="col-md-6 col-xl-5 text-secondary">
              <div class="inset-md-right-15 inset-xl-right-0">
                <p>Agencia destinos nacio en el año 2005 con fines de brindar oportunidades y paquetes de viajes a nuestros clientes, con el transcurrir del tiempo se volvio indispensable el ofrecer el servicio de consultoria y asesoria para obtener los diferentes tipo de visados a las personas para poder realizar sus viajes, desde ese momento agencia destinos a capacitado a personal para que sean asesores legales en los tramites de visados.</p>
                <p>
                  
                  
                </p>
              </div>
            </div>
            <div class="col-md-6">
              <ul class="list-progress">
                <li>
                  <p class="animated fadeIn">Adaptabilidad</p>
                  <div class="progress-bar-js progress-bar-horizontal progress-bar-sisal" data-value="70" data-stroke="4" data-easing="linear" data-counter="true" data-duration="1000" data-trail="100"></div>
                </li>
                <li>
                  <p class="animated fadeIn">Visados familiares</p>
                  <div class="progress-bar-js progress-bar-horizontal progress-bar-laser" data-value="80" data-stroke="4" data-easing="linear" data-counter="true" data-duration="1000" data-trail="100"></div>
                </li>
                <li>
                  <p class="animated fadeIn">Experiencia en visados</p>
                  <div class="progress-bar-js progress-bar-horizontal progress-bar-fuscous-gray" data-value="87" data-stroke="4" data-easing="linear" data-counter="true" data-duration="1000" data-trail="100"></div>
                </li>
                <li>
                  <p class="animated fadeIn">Viajes y turismo</p>
                  <div class="progress-bar-js progress-bar-horizontal progress-bar-leather" data-value="90" data-stroke="4" data-easing="linear" data-counter="true" data-duration="1000" data-trail="100"></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <section class="section-60 section-lg-90 bg-whisper">
        <div class="container">
          <div class="row row-40 align-items-sm-end">
            <div class="col-sm-6 col-md-4 col-lg-3">
              <div class="thumbnail-variant-2-wrap">
                <div class="thumbnail thumbnail-variant-2">
                  <figure class="thumbnail-image"><img src="" alt="" width="246" height="300"/>
                  </figure>
                  <div class="thumbnail-inner">
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary material-icons-local_phone"></span><a class="link-white" href="tel:#">+1 (320) 419 9366</a></div>
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary fa-envelope-o"></span><a class="link-white" href="mailto:#">ventas@agenciadestinos.net</a></div>
                  </div>
                  <div class="thumbnail-caption">
                    <p class="text-header"><a href="#">Patricia Rojas</a></p>
                    <div class="divider divider-md bg-teak"></div>
                    <p class="text-caption">Gerente</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
              <div class="thumbnail-variant-2-wrap">
                <div class="thumbnail thumbnail-variant-2">
                  <figure class="thumbnail-image"><img src="" alt="" width="246" height="300"/>
                  </figure>
                  <div class="thumbnail-inner">
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary material-icons-local_phone"></span><a class="link-white" href="tel:#">+1 (320) 419 9366</a></div>
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary fa-envelope-o"></span><a class="link-white" href="mailto:#">ventas@agenciadestinos.net</a></div>
                  </div>
                  <div class="thumbnail-caption">
                    <p class="text-header"><a href="#">Brialan Gracia</a></p>
                    <div class="divider divider-md bg-teak"></div>
                    <p class="text-caption">Asesor</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
              <div class="thumbnail-variant-2-wrap">
                <div class="thumbnail thumbnail-variant-2">
                  <figure class="thumbnail-image"><img src="" alt="" width="246" height="300"/>
                  </figure>
                  <div class="thumbnail-inner">
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary material-icons-local_phone"></span><a class="link-white" href="tel:#">+1 (320) 419 9366</a></div>
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary fa-envelope-o"></span><a class="link-white" href="mailto:#">ventas@agenciadestinos.net</a></div>
                  </div>
                  <div class="thumbnail-caption">
                    <p class="text-header"><a href="#">Diana Valencia</a></p>
                    <div class="divider divider-md bg-teak"></div>
                    <p class="text-caption">Asistente</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-12 col-lg-3 text-center">
              <div class="block-wrap-1">
                <div class="block-number">06</div>
             <h3 class="text-normal">Expertos</h3>
                <p class="h5 h5-smaller text-style-4">Asesoria Proesional</p>
                <p>Asesores totalmente capacitados en todos los temas de migracion para viajar a distintos paises.</p><a class="link link-group link-group-animated link-bold link-secondary" href="#"><span>Leer mas</span><span class="novi-icon icon icon-xxs icon-primary fa fa-angle-right"></span></a>
              </div>
            </div>
          </div>
        </div>
      </section>

     <!--  <section class="section parallax-container bg-black" data-parallax-img="images/clients-testimonials-parallax-1.jpg">
        <div class="parallax-content">
          <div class="section-60 section-md-90 section-xl-120">
            <div class="container text-center">
              <div class="row row-40 justify-content-lg-center">
                <div class="col-sm-12">
                  <h3>What People Say</h3>
                </div>
                <div class="col-lg-11 col-xl-9">
                  <div class="owl-carousel-inverse">
                    <div class="owl-carousel owl-nav-position-numbering" data-autoplay="true" data-items="1" data-stage-padding="0" data-loop="false" data-margin="30" data-nav="true" data-numbering="#owl-numbering-1" data-animation-in="fadeIn" data-animation-out="fadeOut">
                      <div class="item">
                              <blockquote class="quote-minimal quote-minimal-inverse">
                                <div class="quote-body">
                                  <p>
                                    <q>We have been very pleased with our experience with the LawExpert. Although we did not actually go to court, I felt that Amanda was more than ready to fight her best for us. Our whole experience with all of the staff was very easy and enjoyable. Thank you for your work!</q>
                                  </p>
                                </div>
                                <div class="quote-meta">
                                  <cite>Mark Wilson</cite>
                                  <p class="caption">Client</p>
                                </div>
                              </blockquote>
                      </div>
                      <div class="item">
                              <blockquote class="quote-minimal quote-minimal-inverse">
                                <div class="quote-body">
                                  <p>
                                    <q>I am so glad we chose LawExpert law firm to represent us. We were in a very bad motorcycle accident. We were treated like family and were kept involved every step of the way. Thank you all who were involved in one way or other working our case!</q>
                                  </p>
                                </div>
                                <div class="quote-meta">
                                  <cite>Kate Wilson</cite>
                                  <p class="caption">Client</p>
                                </div>
                              </blockquote>
                      </div>
                      <div class="item">
                              <blockquote class="quote-minimal quote-minimal-inverse">
                                <div class="quote-body">
                                  <p>
                                    <q>John and his staff were great with making us feel comfortable during the process. They kept us updated on our case progress and were very helpful with all of the paperwork we needed to complete. We are very pleased with the outcome of everything.</q>
                                  </p>
                                </div>
                                <div class="quote-meta">
                                  <cite>Sam Cole</cite>
                                  <p class="caption">Client</p>
                                </div>
                              </blockquote>
                      </div>
                    </div>
                    <div class="owl-numbering owl-numbering-default" id="owl-numbering-1">
                      <div class="numbering-current"></div>
                      <div class="numbering-separator"></div>
                      <div class="numbering-count"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> -->

      <section class="bg-whisper">
        <div class="container">
          <div class="row align-items-lg-end">
            <div class="offset-md-1 offset-lg-0 col-md-8 col-lg-6 col-xl-5">
              <div class="section-60 section-md-90">
                <div class="inset-md-left-100">
                  <h3>Servicios y Visados</h3>
                </div>
                <div class="inset-md-left-30 inset-md-right-30">
                  <ul class="list-xl">
                    <li>
                      <article class="icon-box-horizontal">
                        <div class="unit unit-horizontal unit-spacing-md">
                          <div class="unit-left"><span class="novi-icon icon icon-primary icon-lg mercury-icon-users"></span></div>
                          <div class="unit-body">
                            <h5><a href="#">Visa Reino Unido</a></h5>
                            <p>Asesoria profesional para realizar el tramite a Reino Unido.</p>
                          </div>
                        </div>
                      </article>

                    </li>
                    <li>
                      <article class="icon-box-horizontal">
                        <div class="unit unit-horizontal unit-spacing-md">
                          <div class="unit-left"><span class="novi-icon icon icon-primary icon-lg mercury-icon-lib"></span></div>
                          <div class="unit-body">
                            <h5><a href="#">Visa Canadiense</a></h5>
                            <p>Consultores profesionales para solicitar su visa Canadiense.</p>
                          </div>
                        </div>
                      </article>

                    </li>
                    <li>
                      <article class="icon-box-horizontal">
                        <div class="unit unit-horizontal unit-spacing-md">
                          <div class="unit-left"><span class="novi-icon icon icon-primary icon-lg mercury-icon-briefcase"></span></div>
                          <div class="unit-body">
                            <h5><a href="#">Visa EEUU</a></h5>
                            <p>Que tipo de visa necesito para viajar a estados unidos? esto y mas.</p>
                          </div>
                        </div>
                      </article>

                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-9 col-lg-6 col-xl-7 offset-top-30 offset-md-top-0 d-none d-lg-block">
              <div class="image-wrap-1"><img src="<?php echo $base_url ?>asesorias/images/imagenes/4-uk.jpg" alt="" width="670" height="578"/>
              </div>
            </div>
          </div>
        </div>
      </section>

   <?php require __DIR__.'/footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>