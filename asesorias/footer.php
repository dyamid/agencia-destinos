<footer class="page-foot bg-ebony-clay">
        <div class="section-40 section-md-75">
          <div class="container">
            <div class="row justify-content-sm-center">
              <div class="col-sm-9 col-md-11 col-xl-12">
                <div class="row row-50">
                  <div class="col-md-6 col-lg-10 col-xl-3">
                    <div class="inset-xl-right-20" style="max-width: 510px;"><a class="brand" href="index.html"><img src="<?php echo $base_url ?>iconos/logopngicono.png" alt="" width="143" height="28"/></a>
                      <p>
                        If you or your business is facing a legal
                        challenge that calls for sound advice and skilled representation, contact us today to arrange a free  consultation with an attorney.
                      </p><a class="link link-group link-group-animated link-bold link-white" href="#"><span>Free Consultation</span><span class="novi-icon icon icon-xxs icon-primary fa fa-angle-right"></span></a>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-4 col-xl-3">
                    <p class="h7">Preguntas</p>
                          <article class="post post-preview post-preview-inverse"><a href="#">
                              <div class="unit unit-horizontal unit-spacing-lg">
                                <div class="unit-left">
                                  <figure class="post-image"><img style="max-width: 70px" src="<?php echo $base_url ?>asesorias/images/imagenes/4-uk.jpg" alt="" width="70" height="70"/>
                                  </figure>
                                </div>
                                <div class="unit-body">
                                  <div class="post-header">
                                    <p>Requisitos Visa Americana</p>
                                  </div>
                                  <div class="post-meta">
                                    <ul class="list-meta">
                                      <li>
                                        <time datetime="2019-06-23">Junio 23, 2019 </time>
                                      </li>
                                      <li>3 Comments</li>
                                    </ul>
                                  </div>
                                </div>
                              </div></a></article>
                          <article class="post post-preview post-preview-inverse"><a href="#">
                              <div class="unit unit-horizontal unit-spacing-lg">
                                <div class="unit-left">
                                  <figure class="post-image"><img style="max-width: 70px" src="<?php echo $base_url ?>asesorias/images/imagenes/3-uk.jpeg" alt="" width="70" height="70"/>
                                  </figure>
                                </div>
                                <div class="unit-body">
                                  <div class="post-header">
                                    <p>Requisitos Visa Canadiense </p>
                                  </div>
                                  <div class="post-meta">
                                    <ul class="list-meta">
                                      <li>
                                        <time datetime="2019-06-23">Junio 20, 2019</time>
                                      </li>
                                      <li>3 Comments</li>
                                    </ul>
                                  </div>
                                </div>
                              </div></a></article>
                  </div>
                  <div class="col-md-6 col-lg-4 col-xl-3">
                    <p class="h7">Menu</p>
                    <div class="row" style="max-width: 270px;">
                      <div class="col-6">
                        <ul class="list-marked-variant-2">
                          <li><a href="index.html">Inicio</a></li>
                          <li><a href="#">Servicios</a></li>
                          <!-- <li><a href="#">Preguntas</a></li> -->
                          <li><a href="#">Preguntas</a></li>
                        </ul>
                      </div>
                      <div class="col-6">
                        <ul class="list-marked-variant-2">
                          <li><a href="about-us.html">Nosotros</a></li>
                          <li><a href="contact-us.html">Contactenos</a></li>
                          <li><a href="#">Llamar</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-4 col-xl-3">
                    <p class="h7">Contactenos</p>
                    <address class="contact-info text-left">
                      <div class="unit unit-horizontal unit-spacing-md align-items-center">
                        <div class="unit-left"><span class="novi-icon icon icon-xs icon-storm-gray material-icons-phone"></span></div>
                        <div class="unit-body"><a class="link-white" href="tel:#">+57 320 419 93 66</a></div>
                      </div>
                      <div class="unit unit-horizontal unit-spacing-md align-items-center">
                        <div class="unit-left"><span class="novi-icon icon icon-xs icon-storm-gray fa fa-envelope-o"></span></div>
                        <div class="unit-body"><a class="link-white" href="mailto:#">ventas@agenciadestinos.net</a></div>
                      </div>
                      <div class="unit unit-horizontal unit-spacing-md">
                        <div class="unit-left"><span class="novi-icon icon icon-xs icon-storm-gray material-icons-place"></span></div>
                        <div class="unit-body"><a class="link-white d-inline" href="#">,<br>Ibague, Colombia </a></div>
                        <div class="unit-body"><a class="link-white d-inline" href="#">,<br>Bogota, Colombia </a></div>
                      </div>
                    </address>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <hr>
        </div>
        <div class="section-35">
          <div class="container text-center">
            <div class="row row-15 flex-md-row-reverse justify-content-md-between align-items-md-center">
              <div class="col-md-6 text-md-right">
                <div class="group-sm group-middle">
                  <p class="font-italic text-white">Siguenos:</p>
                  <ul class="list-inline list-inline-reset">
                    <li><a class="novi-icon icon icon-circle icon-bright-gray-filled icon-xxs-smaller fa fa-facebook" href="#"></a></li>
                    <li><a class="novi-icon icon icon-circle icon-bright-gray-filled icon-xxs-smaller fa fa-twitter" href="#"></a></li>
                    <li><a class="novi-icon icon icon-circle icon-bright-gray-filled icon-xxs-smaller fa fa-google-plus" href="#"></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-6 text-md-left">
                <p class="rights text-white"><span class="copyright-year"></span><span>&nbsp;&#169;&nbsp;</span><span>Agencia Destinos.&nbsp; All Rights Reserved.</span>Diseño&nbsp;by&nbsp;<a href="https://www.templatemonster.com">Gracia´s Devs</a></p>
              </div>
            </div>
          </div>
        </div>
      </footer>