<!DOCTYPE html>
<html lang="en">
<head>
    <title>Servicios | Embajada Americana</title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="all">  
    <script src="js/jquery-1.6.3.min.js" type="text/javascript"></script>
    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script src="js/Kozuka_Gothic_Pro_OpenType_300.font.js" type="text/javascript"></script>
    <script src="js/Kozuka_Gothic_Pro_OpenType_500.font.js" type="text/javascript"></script>
	<script src="js/Kozuka_Gothic_Pro_OpenType_700.font.js" type="text/javascript"></script>
    <script src="js/Kozuka_Gothic_Pro_OpenType_900.font.js" type="text/javascript"></script> 
    <script src="js/FF-cash.js" type="text/javascript"></script>     
    <script src="js/easyTooltip.js" type="text/javascript"></script> 
    <script src="js/script.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/css3-mediaqueries.js"></script>
	<!--[if lt IE 7]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
        	<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
	<![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
        <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->
</head>
<body id="page4">
	<!--==============================header=================================-->
    <header>
    	<div class="main zerogrid">
            <h1><a href="index.html"><img src="images/logo.png" /></a></h1>
            <nav>
                <ul class="menu">
                    <li><a href="http://www.agenciadestinos.net">Visa Americana</a></li>
					<li><a href="index.html#aqui">Precios</a></li>
                    <!--  <li><a href="company.html">company</a></li>  -->
                    <li><a class="active" href="viajes.html">Viajes</a></li>
                    <li><a href="contacts.html">contactenos</a></li>
                </ul>
            </nav>
            <div class="clear"></div>
        </div>
        <div class="slider-wrapper">
        	<div class="slider">
            	<div class="banner">
                    <strong>VIAJE<strong>CON NOSOTROS!</strong></strong>
                    <em>El mejor servicio  y los mejores precios para sus viajes !</em>
                </div>
            </div>
        </div>
    </header>
    
	<!--==============================content================================-->
    <section id="content">
        <div class="main zerogrid">
                <div class="row">
                	<article class="col-1-4">
						<div class="wrap-col">
	                    	<h3 class="p0">Servicios</h3>
	                        <ul class="list-2">
	                        	<li><a href="contacts.html">Tiquetes </a></li>
	                            <li><a href="contacts.html">Hoteles</a></li>
	                            <li><a href="contacts.html">Paquetes Empresas </a></li>
	                            <li><a href="contacts.html">Cruceros</a></li>
	                            <li><a href="contacts.html">Visas</a></li>
	                            <li><a href="contacts.html">Asesoria en Viajes </a></li>
	                            <li><a href="contacts.html">Actividades</a></li>
	                            <li><a href="contacts.html">Restaurantes</a></li>
	                            <li><a href="contacts.html">Alquiler de Vehiculo </a></li>
	                            <li><a href="contacts.html">Spas y relajación </a></li>
	                            <li><a href="contacts.html">New York y Miami </a></li>
	                            <li><a href="contacts.html">Viaje por  del mundo </a></li>
	                            <li><a href="index.html">Visa Americana </a></li>
	                            <li><a href="index.html">Embajada Americana </a></li> 
	                        </ul>
						</div>
                    </article>
                    <article class="col-3-4">
						<div class="wrap-col">
	                    	<h3>Todos los Destinos</h3>
	                        <div class="wrapper">
	                        	<article class="col-1-3">
									<div class="wrap-col">
		                            	<figure class="p2"><a href="contacts.html"><img class="img-border" src="images/page4-img1.jpg" alt=""></a></figure>
		                                <strong>Air Europa </strong>
		                                <p class="p0">Para Europa y dsde Europa para el mundo </p>
		                                <a class="link" href="contacts.html">Contactenos</a>
									</div>
	                            </article>
	                            <article class="col-1-3">
									<div class="wrap-col">
		                            	<figure class="p2"><a href="contacts.html"><img class="img-border" src="images/page4-img2.jpg" alt=""></a></figure>
		                                <strong>Avianca / Taca </strong>
		                                <p class="p0">Todos los destinos del mundo desde America </p>
		                                <a class="link" href="contacts.html">Contactenos</a>
									</div>
	                            </article>
	                            <article class="col-1-3">
									<div class="wrap-col">
		                            	<figure class="p2"><a href="contacts.html"><img class="img-border" src="images/page4-img3.jpg" alt=""></a></figure>
		                                <strong>Lan</strong>
		                                <p class="p0">Todos los destinos a los mejores precios </p>
		                                <a class="link" href="contacts.html">Contactenos</a>
									</div>
	                            </article>
	                        </div>
	                        <div class="wrapper">
	                        	<article class="col-1-3">
									<div class="wrap-col">
		                            	<figure class="p2"><a href="contacts.html"><img class="img-border" src="images/page4-img4.jpg" alt=""></a></figure>
		                                <strong>Ski Lankan </strong>
		                                <p class="p0">Australia y Oceania al alcance</p>
		                                <a class="link" href="contacts.html">Contactenos</a>
									</div>
	                            </article>
	                            <article class="col-1-3">
									<div class="wrap-col">
		                            	<figure class="p2"><a href="contacts.html"><img class="img-border" src="images/page4-img5.jpg" alt=""></a></figure>
		                                <strong>South African AirWays </strong>
		                                <p class="p0">Todos los destinos de Sudafrica </p>
		                                <a class="link" href="contacts.html">Contactenos</a>
									</div>
	                            </article>
	                            <article class="col-1-3">
									<div class="wrap-col">
		                            	<figure class="p2"><a href="contacts.html"><img class="img-border" src="images/page4-img6.jpg" alt=""></a></figure>
		                                <strong>Cathay Pacific </strong>
		                                <p class="p0">China y toda Asia </p>
		                                <a class="link" href="contacts.html">Contactenos</a>
									</div>
	                            </article>
	                        </div>
						</div>
                    </article>
                </div>
        </div>
    </section>
    
	<!--==============================footer=================================-->
    <footer>
        <div class="main zerogrid">
        	<div class="row">
            	<article class="col-1-4">
					<div class="wrap-col">
                    	<ul class="list-services">
                        	<li class="item-1"><a class="tooltips" title="facebook" href="#"></a></li>
                            <li class="item-2"><a class="tooltips" title="twiiter" href="#"></a></li>
                            <li class="item-3"><a class="tooltips" title="delicious" href="#"></a></li>
                            <li class="item-4"><a class="tooltips" title="youtube" href="#"></a></li>
                        </ul>
					</div>
                </article>
                <article class="col-1-4">
                	<div class="wrap-col">
                        <h5>Navigation</h5>
                        <ul class="list-1">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="company.html">Company</a></li>
                            <li><a href="index.html#aqui">Precios</a></li>
                            <li><a href="viajes.html">Travel</a></li>
                            <li><a href="contacts.html">Contactenos</a></li>
                        </ul>
                    </div>
                </article>
                <article class="col-1-4">
					<div class="wrap-col">
                    	<h5>Contact</h5>
                        <dl class="contact">
                            <dt>Visasegura.com <br>
                            tramites@visasegura.com
                            <br>
                              Agencia de Viajes Destinos </dt>
                            <dd><span>Movistar:</span> 315-8814839  </dd>
                            <dd><span>Comcel:</span>  320-4199366</dd>
                      </dl>
					 </div>
                </article>
                <article class="col-1-4">
					<div class="wrap-col">
                    	<h5>Legal</h5>
                        <p class="prev-indent-bot3 color-1">Visa Segura &copy; 2002</p>
                        <p class="prev-indent-bot3"><a class="link" target="_blank" href="http://www.zerotheme.com/">Html5 Templates</a></p>
                        <p class="color-1 p0">by <a class="link" target="_blank" href="http://www.templatemonster.com/">TemplateMonster</a></p>
					</div>
                </article>
            </div>
        </div>
    </footer>
	<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>
