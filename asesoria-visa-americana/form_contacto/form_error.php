<?php require_once('config.php'); ?>
<html>
<head>
<title>Formulario de contacto - Error</title>
<link href='styles.css' rel='stylesheet' type='text/css'>

<script language='JavaScript' type='text/JavaScript'>
function vacio(q) {  
       for ( i = 0; i < q.length; i++ ) {  
                if ( q.charAt(i) != ' ' ) {  
                        return true  
                }  
        }  
         return false  
 }

function valida(formulario){
  // primera comprobacion
  if(formulario.Nombre.value == ''){
  alert('Debe introducir su nombre y apellidos o Empresa');
  formulario.Nombre.focus();
  return false;
  }
  if( vacio(formulario.Nombre.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para su nombre')  
  formulario.Nombre.focus();
  return false;
  }
  // fin
  
  // segunda comprobacion
  if(formulario.Email.value == ''){
  alert('Debe introducir su direccion de email');
  formulario.Email.focus();
  return false;
  }
  if( vacio(formulario.Email.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para su email')  
  formulario.Email.focus();
  return false;
  }
  // fin
  
    // tercera comprobacion
  if(formulario.Asunto.value == ''){
  alert('Debe introducir el asunto de su mensaje');
  formulario.Asunto.focus();
  return false;
  }
  if( vacio(formulario.Asunto.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para el asunto')  
  formulario.Asunto.focus();
  return false;
  }
  // fin
  
      // cuarta comprobacion
  if(formulario.Mensaje.value == ''){
  alert('Debe introducir el mensaje');
  formulario.Mensaje.focus();
  return false;
  }
  if( vacio(formulario.Mensaje.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para el mensaje')  
  formulario.Mensaje.focus();
  return false;
  }
  // fin

  // quinta comprobacion
  if(formulario.Codigo.value == ''){
  alert('Debe copiar el codigo anti SPAM mostrado en la imagen');
  formulario.Codigo.focus();
  return false;
  }
  if( vacio(formulario.Codigo.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para el codigo anti SPAM')  
  formulario.Codigo.focus();
  return false;
  }
  // fin
  return true;
}
</script>

</head>
<body style='background-color: transparent;'>
<div align='center'>
<table width='352' border='0' cellspacing='0' cellpadding='0'>
<tr>
<td class='descdet'><strong>Por favor complete el formulario</strong> 
<div class='bordeder'>
 
<form action='enviar.php' method='post' enctype='multipart/form-data' name='formulario' target='_self' id='formulario' onSubmit='return valida(this)'>
<div id='campos'>

Su Nombre<br>
<input name='Nombre' type='text' id='Nombre' size='52' maxlength='50'><br><br>

Su Email<br>
<input name='Email' type='text' id='Email' size='52' maxlength='50'><br><br>

Asunto<br>
<input name='Asunto' type='text' id='Asunto' size='52' maxlength='50'><br><br>

Mensaje<br>
<textarea style="resize: none" name='Mensaje' cols='50' rows='6' id='Mensaje' type='text'></textarea>
<div style="padding-top:2px;max-height:34px"  align="center"><strong style='color:red;font-size:11px'>El codigo no es correcto!</strong><br />
                Copie debajo el codigo anti SPAM</div>
<img src='captcha.php' width='50' height='30' vspace='3'>
<br>
<input name='Codigo' type='text' size='6' maxlength='4'></div>
<br>
<input name='Action' type='hidden' value='Check'>

<div id='boton_grande'><input type='submit' name='enviar' value='Enviar'></div></form>
</div>

</td>
</tr>
</table>
</div>
</body>
</html>