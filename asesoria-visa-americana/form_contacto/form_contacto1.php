<?php require_once('config.php'); ?>
<html>
<head>
<title>Formulario de contacto</title>
<link href='styles.css' rel='stylesheet' type='text/css'>

<script language='JavaScript' type='text/JavaScript'>
function vacio(q) {  
       for ( i = 0; i < q.length; i++ ) {  
                if ( q.charAt(i) != ' ' ) {  
                        return true  
                }  
        }  
         return false  
 }

function valida(formulario){
  // primera comprobacion
  if(formulario.Nombre.value == ''){
  alert('Debe introducir su nombre y apellidos o Empresa');
  formulario.Nombre.focus();
  return false;
  }
  if( vacio(formulario.Nombre.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para su nombre')  
  formulario.Nombre.focus();
  return false;
  }
  // fin
  
  // segunda comprobacion
  if(formulario.Email.value == ''){
  alert('Debe introducir su direccion de email');
  formulario.Email.focus();
  return false;
  }
  if( vacio(formulario.Email.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para su email')  
  formulario.Email.focus();
  return false;
  }
  // fin
  
    // tercera comprobacion
  if(formulario.Asunto.value == ''){
  alert('Debe introducir el nombre de su mensaje');
  formulario.Asunto.focus();
  return false;
  }
  if( vacio(formulario.Asunto.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para el asunto')  
  formulario.Asunto.focus();
  return false;
  }
  // fin
  
      // cuarta comprobacion
  if(formulario.Mensaje.value == ''){
  alert('Debe introducir el mensaje');
  formulario.Mensaje.focus();
  return false;
  }
  if( vacio(formulario.Mensaje.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para el mensaje')  
  formulario.Mensaje.focus();
  return false;
  }
  // fin

  // quinta comprobacion
  if(formulario.Codigo.value == ''){
  alert('Debe copiar el codigo anti SPAM mostrado en la imagen');
  formulario.Codigo.focus();
  return false;
  }
  if( vacio(formulario.Codigo.value) == false ) {  
  alert('Solo espacios en blanco no estan permitidos para el codigo anti SPAM')  
  formulario.Codigo.focus();
  return false;
  }
  // fin
  return true;
}
</script>

</head>
<body style='background-color: transparent;'>
<div align='center'>
<table width='400' border='0' cellspacing='0' cellpadding='0' bgcolor="#FFFFFF">
<tr>
<td>

<form action='enviar.php' method='post' enctype='multipart/form-data' name='formulario' target='_self' id='formulario' onSubmit='return valida(this)'>

<table width="100%" border="0" cellpadding="4" cellspacing="1">
  <tr>
    <td width="33%" align="Right"><b id="letra">Nombre: <span class="style1">*</span></b></td>
    <td width="67%"><input name='Nombre' type='text' id='Nombre' size='40' maxlength='50'></td>
  </tr>
  <tr>
  <td align="Right"><b id="letra">Correo: <span class="style1">*</span></b></td>
    <td><input name='Email' type='text' id='Email' size='40' maxlength='50'></td>
  </tr>
  <tr>
    <td align="Right"><b id="letra">Telefono:</b></td>
    <td><input name='Asunto' type='text' id='Asunto' size='40' maxlength='50'></td>
  </tr>
  <tr>
    <td align="Right"><b id="letra">Datos de pago: <span class="style1">*</span></b></td>
    <td rowspan="2"><textarea style="resize: none" name='Mensaje' cols='40' rows='3' id='Mensaje' type='text'>Ingrese AQUI numero de consiganaci&oacute;n o pago, especifique el medio (Banco, giro o tarjeta).</textarea>
    <div id="letra" align="center">Copie debajo el codigo 
                anti SPAM</div>
<center><img src='captcha.php' width='50' height='30' vspace='3'>
<br>
<input name='Codigo' type='text' size='6' maxlength='4'>
<br>
<input name='Action' type='hidden' value='Check'>

<div id='boton_grande'><input type='submit' name='enviar' value='Enviar'></div></center></td>
  </tr>
  <tr>
    <td align="Right">&nbsp;</td>
  </tr>
</table>
</form>

</td>
</tr>
</table>
</div>
</body>
</html>