﻿<!DOCTYPE html>
<html lang="en">
<head>
    <title>tramite visa americana | Embajada americana</title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">  
	<link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="all">
    <script src="js/jquery-1.6.3.min.js" type="text/javascript"></script>
    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script src="js/Kozuka_Gothic_Pro_OpenType_300.font.js" type="text/javascript"></script>
    <script src="js/Kozuka_Gothic_Pro_OpenType_700.font.js" type="text/javascript"></script>
    <script src="js/Kozuka_Gothic_Pro_OpenType_900.font.js" type="text/javascript"></script> 
    <script src="js/FF-cash.js" type="text/javascript"></script>     
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/tms-0.3.js"></script>
	<script type="text/javascript" src="js/tms_presets.js"></script>
    <script type="text/javascript" src="js/easyTooltip.js"></script> 
    <script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/css3-mediaqueries.js"></script>
	<!--[if lt IE 7]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
        	<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
	<![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
        <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->
	
	<meta name="description" content="Asesoria a nivel nacional e internacional para solicitud de visa americana ante la embajada americana de los Estados unidos, visa de turismo, tramites para la entrevista formulario DS-160"/>
<link rel="canonical" href="http://destinostolima.net" />
<link rel="author" href="https://plus.google.com/u/0/102675234191410807723"/>
<link rel="publisher" href="https://plus.google.com/109432741268893856558"/>
<meta property="locale" content="es_ES" />
<meta property="type" content="article" />
<meta property="title" content="TRAMITE VISA AMERICANA EMBAJADA AMERICANA" />
<meta property="site_name" content="destinostolima.net" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55385945-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body id="page1">
	<!--==============================header=================================-->
    <header>
    	<div class="main zerogrid">
        	<div class="prev-indent-bot2">
                <h1><a href="../index.php"><img src="images/logo2.jpg" /></a></h1>
                <nav>
                    <ul class="menu">
                        <li><a class="active" href="index.php">Visa Americana</a></li>
						<li><a href="#aqui">Precios</a></li>
                        <!-- <li><a href="company.html">company</a></li> -->
                         <li><a href="http://www.agenciadestinos.net/">Viajes</a></li>
                        <li><a href="http://www.agenciadestinos.net/contactenos/">contactenos</a></li>
                    </ul>
                </nav>
                <div class="clear"></div>
            </div>
        </div>
        <div class="slider-wrapper">
        	<div class="slider">
            	<ul class="items">
                	<li>
                    	<img src="images/slider-img1.jpg" alt="">
                        <div class="banner">
                        	<strong>necesita <strong>tramitar su visa ?</strong></strong>
                            <em>Nosotros lo hacemos por usted. Rapido, confiable y economico!</em>
                            <span class="button">
                            	<a href="#aqui"><strong>ver mas  aqui </strong></a>                            </span>                        </div>
                    </li>
                    <li>
                    	<img src="images/slider-img2.jpg" alt="">
                        <div class="banner">
                        	<strong>Excelentes <strong>Resultados!</strong></strong>
                            <em>Le guiamos durante todo su tramite ante la Embajada Americana!</em>
                            <span class="button">
                            	<a href="#aqui"><strong>ver mas  aqui</strong></a>
                            </span>
                        </div>
                    </li>
                    <li>
                    	<img src="images/slider-img3.jpg" alt="">
                        <div class="banner">
                        	<strong>SOMOS<strong>EXPERTOS!</strong></strong>
                            <em>Contactenos y deje su solicitud de Visa Americana en nuestras manos !</em>
                            <span class="button">
                            	<a href="#aqui"><strong>ver mas  aqui</strong></a>
                            </span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    
	<!--==============================content================================-->
    <section id="content">
	
        <div class="main zerogrid">
				 <div class="row">
                	<article class="col-3-4">
						<div class="wrap-col"><a name="aqui"></a>
	                    	<h3 align="center">Asesoria y Tramite para Visa Americana </h3>
							<div class="wrap-col">
						<h5 align="center">El tramite completo por persona individual es de $80.000. <br/>
Para grupos de 3 personas en adelante el valor es de solo $70.000 por cada uno.</h5>
</div>
						</div>
                    </article>
                    <article class="col-1-4">
						<div class="wrap-col">
	                    	<div class="wrapper img-indent-bot">
	                            <figure class="map-border">
	                               <a href="http://www.agenciadestinos.net/contactenos/"> <img src="images/llamenos.png" border="0" /></a>
	                            </figure>
	                        </div>
						</div>
                    </article>
                </div>
                <div class="row">
                	<article class="col-full">
                        <div class="row">
                        	<article class="col-1-3">
                            	<div class="wrap-col">
                                	<figure class="img-indent2"><img src="images/page3-img1.png" alt=""></figure>
                                    <div class="extra-wrap">
                                    	<strong class="title-1">Revisamos su caso</strong>
                                        Descargue nuestra hoja de calificación exclusiva para clientes, conteste las preguntas y reciba los mejores consejos de los expertos para que aumente sus posibilidades ante la embajada americana en el momento del tramite de su visa.
                                    </div>
                                </div>
                            </article>
                            <article class="col-1-3">
                            	<div class="wrap-col">
                                    <figure class="img-indent2"><img src="images/page3-img4.png" alt=""></figure>
                                    <div class="extra-wrap">
                                        <strong class="title-1">Formulario DS-160</strong>
                                        El formulario para la solicitud en la Embajada Americana es algo tedioso, pero no se preocupe a traves de nuestra asesoria para el tramaite de su visa, 	hacemos fácil todo el proceso para usted o su familia.
                                    </div>
                              </div>
                            </article>
                            <article class="col-1-3">
                            	<div class="wrap-col">
                                    <figure class="img-indent2"><img src="images/page3-img3.png" alt=""></figure>
                                    <div class="extra-wrap">
                                        <strong class="title-1">Itinerarios de viaje</strong>
                                        Le brindamos su itinerario de viaje y vuelos para la solicitud de su visa americana, todo sin costo adicional, además cotizamos su viaje y le damos los mejores precios y ofertas.
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="row">
                        	<article class="col-1-3">
                            	<div class="wrap-col">
                                	<figure class="img-indent2"><img src="images/page3-img5.png" alt=""></figure>
                                    <div class="extra-wrap">
                                    	<strong class="title-1">Visa Familiar</strong>
                                        Si desea solicitar la visa americana familiar nosotros realizamos el tramite para cada solicitante y les agendamos una sola cita en grupo indicandoles los requisitos para su visa americana ante la embajada.                                    </div>
                                </div>
                            </article>
                            <article class="col-1-3">
                            	<div class="wrap-col">
                                    <figure class="img-indent2"><img src="images/page3-img2.png" alt=""></figure>
                                    <div class="extra-wrap">
                                        <strong class="title-1">Solicitud de cita</strong>
                                        Realizamos el trámite de visa ante la pagina del AIS para inscripción, pago y solicitud de citas para huellas y entrevista de su solicitud de visa americama ante la embajada. </div>
                                </div>
                            </article>
                            <article class="col-1-3">
                            	<div class="wrap-col">
                                    <figure class="img-indent2"><img src="images/page3-img6.png" alt=""></figure>
                                    <div class="extra-wrap">
                                        <strong class="title-1">Económico</strong>
                                        La asesoria y tramite de la visa americana por persona individual es de <strong>$70.000</strong>. <br/>
										Para grupos de 3 personas en adelante el valor es de solo <strong>$60.000</strong> por cada uno. </div>
                                </div>
                            </article>
                        </div>
						<h4 align="center">Para iniciar o recibir informacion sin compromiso</h4>
					  <h3 align="center">Envienos un mensaje dando clic <a href="http://www.agenciadestinos.net/contactenos/">aqui</a> <br/>
						o llamenos al 320 4199366</h3>
                        <h3 class="prev-indent-bot2">Mas Servicios</h3>
                        <div class="row">
                        	<article class="col-1-3">
							  <div class="wrap-col">
	                                <strong class="circle">A</strong>
	                                <div class="extra-wrap">
	                                    <div class="indent-top3">
	                                        <strong class="title-1 color-2">Hagalo usted mismo por solo $25.000 <strong></strong></strong>
	                                    </div>
	                                </div>
	                                <div class="clear"></div>
                                  Si desea puede realizar el tramite de su visa americana con ayuda de nuestros<strong> tutoriales en Video</strong>, donde le guiaremos paso a paso para que diligencie su solicitud o la de su familia, realice los pagos ante la embajada americana y solicite su cita, además haga todas sus preguntas y reciba respuestas avanzadas para nuestros clientes,todo el proceso y requisitos de la visa americana fácil, valor de esta asesoría en videos paso a paso <strong>$25.000.</strong> </div>
                            </article>
                            <article class="col-1-3">
                           	  <div class="wrap-col">
                                    <strong class="circle">B</strong>
                                    <div class="extra-wrap">
                                        <div class="indent-top3">
                                            <strong class="title-1 color-2">Itinerarios y Ofertas en viajes <strong></strong></strong>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                  Si desea cotizar o comprar sus pasajes y estadias en hoteles para Estados Unidos o cualquier lugar del mundo entonces esta en las mejores manos, <strong>somos aliados de despegar.com</strong> y podemos ofrecer <strong>las mejores tarifas del mercado</strong>, además de nuestra asesoria para que sus viajes sean placenteros y sin contratiempos, descuentos especiales para solicitantes de visa americana..                                </div>
                            </article>
                            <article class="col-1-3">
                           	  <div class="wrap-col">
                                    <strong class="circle">C</strong>
                                    <div class="extra-wrap">
                                        <div class="indent-top3">
                                            <strong class="title-1 color-2">Tramite visa Canadiense <strong></strong></strong>                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                  Además de asesorar y ayudarle con los tramites y requisitos para la visa americana también prestamos un servicio profesional para realizar los <strong>tramites para la visa canadiense</strong>, contamos con contactos directamente en Canada que le ayudaran con todos los requisitos, de una manera clara y <strong>confiable</strong> y lo mejor a unos precios asequibles para todos. Contactenos, no espere mas y averigue por este proceso.                          </div>
                            </article>
                        </div>
                    </article>
                </div>
				<?php include ("testimonios.php");?>
        </div>
		
    </section>
    
	<!--==============================footer=================================-->
    <footer>
        <div class="main zerogrid">
        	<div class="row">
            	<article class="col-1-4">
					<div class="wrap-col">
                    	<ul class="list-services">
                        	<li class="item-1"><a class="tooltips" title="facebook" href="#"></a></li>
                            <li class="item-2"><a class="tooltips" title="twiiter" href="#"></a></li>
                            <li class="item-3"><a class="tooltips" title="delicious" href="#"></a></li>
                            <li class="item-4"><a class="tooltips" title="youtube" href="#"></a></li>
                        </ul>
					</div>
                </article>
                <article class="col-1-4">
                	<div class="wrap-col">
                        <h5>Navigation</h5>
                        <ul class="list-1">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="company.html">Company</a></li>
                            <li><a href="index.html#aqui">Precios</a></li>
                            <li><a href="viajes.html">Travel</a></li>
                            <li><a href="contacts.html">Contactenos</a></li>
                        </ul>
                    </div>
                </article>
                <article class="col-1-4">
					<div class="wrap-col">
                    	<h5>Contact</h5>
                        <dl class="contact">
                            <dt>Visasegura.com <br>
                            tramites@visasegura.com
                            <br>
                              Agencia de Viajes Destinos </dt>
                           
                            <dd><span>Comcel:</span>  320-4199366</dd>
                      </dl>
					 </div>
                </article>
                <article class="col-1-4">
					<div class="wrap-col">
                    	<h5>Legal</h5>
                        <p class="prev-indent-bot3 color-1">Visa Segura &copy; 2002</p>
                        <p class="prev-indent-bot3"><a class="link" target="_blank" href="http://www.zerotheme.com/"> Html5 Templates</a></p>
                        <p class="color-1 p0">by <a class="link" target="_blank" href="http://www.templatemonster.com/">TemplateMonster</a></p>
					</div>
                </article>
            </div>
        </div>
    </footer>
	<script type="text/javascript"> Cufon.now(); </script>
    <script type="text/javascript">
		$(window).load(function(){
			$('.slider')._TMS({
				duration:800,
				easing:'easeOutQuad',
				preset:'simpleFade',
				pagination:true,//'.pagination',true,'<ul></ul>'
				pagNums:false,
				slideshow:7000,
				banners:'fade',// fromLeft, fromRight, fromTop, fromBottom
				waitBannerAnimation:false
			})
		})
	</script>
</body>
</html>
