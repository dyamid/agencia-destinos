<!DOCTYPE html>
<html lang="en">
<head>
    <title>Contactenos | visa americana</title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
     <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="all">   
    <script src="js/jquery-1.6.3.min.js" type="text/javascript"></script>
    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script src="js/Kozuka_Gothic_Pro_OpenType_300.font.js" type="text/javascript"></script>
    <script src="js/Kozuka_Gothic_Pro_OpenType_500.font.js" type="text/javascript"></script>
	<script src="js/Kozuka_Gothic_Pro_OpenType_700.font.js" type="text/javascript"></script>
    <script src="js/Kozuka_Gothic_Pro_OpenType_900.font.js" type="text/javascript"></script> 
    <script src="js/FF-cash.js" type="text/javascript"></script>     
    <script src="js/easyTooltip.js" type="text/javascript"></script> 
    <script src="js/script.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/css3-mediaqueries.js"></script>
	<!--[if lt IE 7]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
        	<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
	<![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
        <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->
</head>
<body id="page3">
	<!--==============================header=================================-->
    <header>
    	<div class="main zerogrid">
            <h1><a href="index.html"><img src="images/logo.png" /></a></h1>
            <nav>
                <ul class="menu">
                    <li><a href="index.html">home</a></li>
					<li><a href="index.html#aqui">Precios</a></li>
                    <li><a href="company.html">company</a></li>
                    <!--  <li><a href="viajes.html">Vijes</a></li>  -->
                    <li><a class="active" href="contacts.html">contactenos</a></li>
                </ul>
            </nav>
            <div class="clear"></div>
        </div>
        <div class="slider-wrapper">
        	<div class="slider">
            	<div class="banner">
                    <strong>CONTACTENOS<strong>SIN COMPROMISO!</strong></strong>
                    <em>Una atencion amable esta esperando por usted!</em>
                </div>
            </div>
        </div>
    </header>
    
	<!--==============================content================================-->
    <section id="content">
        <div class="main zerogrid">
                <div class="row">
                	<article class="col-3-4">
						<div class="wrap-col"><a name="marca"></a>
	                    	<h3>Envienos un mensaje</h3>
	                        <!--<form id="contact-form" method="post" enctype="multipart/form-data">                    
	                            <fieldset>
	                                  <label><span class="text-form">Your Name:</span><input type="text"></label>
	                                  <label><span class="text-form">Your Email:</span><input type="text"></label>                              
	                                  <div class="wrapper">
	                                    <div class="text-form">Your Message:</div>
	                                    <div class="extra-wrap">
	                                        <textarea></textarea>
	                                        <div class="clear"></div>
	                                        <div class="buttons">
	                                        	<span class="button-2">
	                                                <a onClick="document.getElementById('contact-form').reset()"><strong>clear</strong></a>
	                                            </span>
	                                            <span class="button-2">
	                                                <a onClick="document.getElementById('contact-form').submit()"><strong>send</strong></a>
	                                            </span>
	                                        </div> 
	                                    </div>
	                                  </div>                            
	                            </fieldset>						
	                        </form>-->
							<div align="center">
<iframe width="100%" height="350" src="form_contacto/form_contacto.php" frameborder="0" AllowTransparency> 
Si ves este mensaje, significa que tu navegador no soporta esta caracteristica o esta deshabilitada
<br>
Puedes acceder al formulario aqui: <a href="form_contacto/form_contacto.php"><strong>Formulario de contacto</strong></a> 
</iframe>
</div>
						</div>
                    </article>
                    <article class="col-1-4">
						<div class="wrap-col">
	                    	<h3>Visa  USA! </h3>
	                        <div class="wrapper img-indent-bot">
	                            <figure class="map-border">
	                                <img src="images/visa.jpg" />
	                            </figure>
	                        </div>
	                        <dl>
	                            <dt class="p2">Llamanos:</dt>
	                            <dd><span>Movistar:</span> 315-8814839</dd>
	                            <dd><span>Comcel:</span> 320-4199366</dd>
	                            <dd><span>Email:</span> <a class="link" href="#">tramites@visasegura.com</a></dd>
	                        </dl>
						</div>
                    </article>
                </div>
        </div>
    </section>
    
	<!--==============================footer=================================-->
    <footer>
        <div class="main zerogrid">
        	<div class="row">
            	<article class="col-1-4">
					<div class="wrap-col">
                    	<ul class="list-services">
                        	<li class="item-1"><a class="tooltips" title="facebook" href="#"></a></li>
                            <li class="item-2"><a class="tooltips" title="twiiter" href="#"></a></li>
                            <li class="item-3"><a class="tooltips" title="delicious" href="#"></a></li>
                            <li class="item-4"><a class="tooltips" title="youtube" href="#"></a></li>
                        </ul>
					</div>
                </article>
                <article class="col-1-4">
                	<div class="wrap-col">
                        <h5>Navigation</h5>
                        <ul class="list-1">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="company.html">Company</a></li>
                            <li><a href="index.html#aqui">Precios</a></li>
                            <li><a href="viajes.html">Travel</a></li>
                            <li><a href="contacts.html">Contactenos</a></li>
                        </ul>
                    </div>
                </article>
                <article class="col-1-4">
					<div class="wrap-col">
                    	<h5>Contact</h5>
                        <dl class="contact">
                            <dt>Visasegura.com <br>
                            tramites@visasegura.com
                            <br>
                              Agencia de Viajes Destinos </dt>
                            <dd><span>Movistar:</span> 315-8814839  </dd>
                            <dd><span>Comcel:</span>  320-4199366</dd>
                      </dl>
					 </div>
                </article>
                <article class="col-1-4">
					<div class="wrap-col">
                    	<h5>Legal</h5>
                        <p class="prev-indent-bot3 color-1">Visa Segura &copy; 2002</p>
                        <p class="prev-indent-bot3"><a class="link" target="_blank" href="http://www.zerotheme.com/">Html5 Templates</a></p>
                        <p class="color-1 p0">by <a class="link" target="_blank" href="http://www.templatemonster.com/">TemplateMonster</a></p>
					</div>
                </article>
            </div>
        </div>
    </footer>
	<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>
