<?php 
session_start ();

if (isset($_POST["lang"])) {
	$lang = $_POST["lang"];
	if(!empty($lang))
		{
		$_SESSION["lang"] = $lang;	
		}
}

if (isset($_SESSION["lang"])) 
{
	$lang= $_SESSION["lang"];
	require "lang/".$lang.".php";
} else {
	require "lang/es.php";
}

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="<?php echo $lang ['descripcion'] ?>">
	<title><?php echo $lang ["paqnac"] ?></title>
	<link rel="stylesheet" href="">
	<link rel="stylesheet" href="1.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="shorcut icon" type="image/x-icon" href="imagenes/logo.png">
	<script type="text/javascript" src="jquery1.js"></script>
	<script src="javs/cojs.js" type="text/javascript" ></script>
<body id="inicio2">
   

<header class=cabecera>
		<div>
			<a href="inicio.php"><img  class="logo" src="iconos/encabezadologo.png" alt="logotipo"  ></a>
			<div class="icosoc">
				<ul class="redso">

					<li class="icored"><a target="_blank" href="https://www.facebook.com/destinos.tolima?fref=ts"><input type="image" name="redsoc" src="imagenes/facebook2.png" ></a></li>
					<li class="icored2"><input type="image" name="redsoc" src="imagenes/whatsapp.png"></li>
					<li class="icored2"><input type="image" name="redsoc" src="imagenes/gorjeo.png"></li>
				</ul>
			</div>
			<form class="uno" method="POST" >

				<input class="bot" name=lang type="image" src="imagenes/columbia.png" value="es">
				<input class="bot" name=lang type="image" src="imagenes/usa.png" value="en">
				<p class="idi">Idioma</p>
			</form>

		</div>
	</header>
	
	

		<ul class="menupag2">
			<li data-go="have" class="dropdown"><a id="btn0"   data-action="open"><?php echo $lang['inicio'] ?><i class="fa fa-arrow-circle-right" id="hoche" style="margin-left:10px"></a></i>

					<ul class="desplega">
							<li><a href="paqueteinternacional.php"><?php echo $lang['paqint'] ?></a></li>
							<li><a href="paquetesibague.php"><?php echo $lang['paqiba'] ?></a></li>
							<li><a href=""><?php echo $lang['visame'] ?></a></li>
							<li><a href=""><?php echo $lang['alqfin'] ?></a></li>
							<li><a href=""><?php echo $lang['vuehot'] ?></a></li>
						</ul>

				</li>

			<a id="btn2"  data-insert="paqueballena" href="#"  data-ac="op"><li><?php echo $lang['verballe'] ?></li></a>
			<a id="btn2"  data-insert="paqueguajira" href="#"  data-ac="op"><li><?php echo $lang['providencia'] ?></li></a>
			<a id="btn2"  data-insert="paqueprovi" href="#"  data-ac="op"><li><?php echo $lang['guajira'] ?></li></a>
			<a id="btn2"  data-insert="paquesanandres" href="#" data-ac="op"><li><?php echo $lang['sanandres'] ?></li></a>			
		</ul>
	
	
	 		<div class="conpaquenac" id="paqueballena" style="display:none">
				<div class="n1ac" id="1pa">	
				<h2 class="n1ac1"><?php echo $lang['titulopaqueteavistamientodeballenas'] ?></h2>
				<p class="n1ac1"><?php echo $lang['paqueteavistamientodeballenas'] ?></p>
				<img src="imapaqnac/avisball1.jpg" alt="">
				<img src="imapaqnac/avisball2.jpg" alt="">		
				</div>
				<div class="video"><iframe class="vide1" width="317" height="170" src="https://www.youtube.com/embed/	brQK_q35-1M" frameborder="0" allowfullscreen></iframe>
				</div>		
			</div>	
			<div class="conpaquenac" id="paqueprovi" style="display:none">
				<div class="n1ac" id="1pa">		
					<h2 class="n1ac1"><?php echo $lang['titupaqueteguajira'] ?></h2>
					<p class="n1ac1"><?php echo $lang['paqueteguajira'] ?></p>
					<img src="imapaqnac/lagua1.jpg" alt="">
					<img src="imapaqnac/lagua2.jpg" alt="">			
				</div>			
				<div class="video"><iframe width="317" height="170" src="https://www.youtube.com/embed/ue_YowcSrGc" frameborder="0" allowfullscreen></iframe>				
				</div>			
			</div>	
			<div class="conpaquenac" id="paqueguajira" style="display:none">
				<div class="n1ac" id="1pa">		
					<h2 class="n1ac1"><?php echo $lang['tituprov'] ?></h2>
					<p class="n1ac1"><?php echo $lang['paqueteprovidencia'] ?></p>
					<img src="imapaqnac/islpro1.jpg" alt="">
					<img src="imapaqnac/islprov2.jpg" alt="">			
				</div>			
				<div class="video"><iframe width="317" height="170" src="https://www.youtube.com/embed/OGhsDJNuDIk?list=PLCnsqE9qGNAeYZek7fjIAsNLTKiz2q1rK" frameborder="0" allowfullscreen></iframe>				
				</div>			
			</div>	
			<div class="conpaquenac" id="paquesanandres" style="display:none">
				<div class="n1ac" id="1pa">		
					<h2 class="n1ac1"><?php echo $lang['titusanandres'] ?></h2>
					<p class="n1ac1"><?php echo $lang['paquetesanandres'] ?></p>
					<img src="imapaqnac/sanand1.jpg" alt="">
					<img src="imapaqnac/sanand2.jpg" alt="">			
				</div>			
				<div class="video"><iframe width="317" height="170" src="https://www.youtube.com/embed/v-maUZF9T-k?list=PLCnsqE9qGNAeYZek7fjIAsNLTKiz2q1rK" frameborder="0" allowfullscreen></iframe>					
				</div>			
			</div>	
		
	</div>		
	<div class="dere" id="polsos" style="display:none">
		<div class="poli">
		<h3><?php echo $lang['polisos'] ?></h3>
		<p class="politica"><?php echo $lang['politica'] ?>			
		</p>
		</div>
	</div>
	<div class="dere" id="polsos1" style="display:none">
		<div class="poli">
		<h3 class="vi"><?php echo $lang['tituvision'] ?></h3>
		<p class="politica"><?php echo $lang['vision'] ?></p>
		<h3 class="vi"><?php echo $lang['titumision'] ?></h3>
		<p class="politica"><?php echo $lang['mision'] ?></p>

		</div>
	</div>
	<div class="dere" id="polsos2" style="display:none">
		<ul class="contactanos">
			<li><a href="https://www.facebook.com/destinos.tolima?fref=ts" title=""><input class="c" type="image" src="imagenes/facebook.png"><p class="c">Destinos Tolima</p></a></li>
			<li><input class="c" type="image" src="imagenes/tecnologia.png"><p class="c">0573204199366</p></li>
			<li><input class="c" type="image" src="imagenes/gmail-1.png"><p class="c"></p></li>
			<li><a  href=""><input class="c" type="image" src="imagenes/whatsapp-1.png"><p class="c">+0573204199366</p></a></li>
		</ul>
	</div>
	<footer id="pie">
			<img class="icologo" id="avion" data-grado="one" src="imagenes/titulo2.png" alt="logotipo" >
		<div class="pies">
			<h3 id="btn1" data-conima="polsos" href="#"  data-action="open" class="titu0"><?php echo $lang['polisos'] ?></h3>
			
			<h3 id="btn1" data-conima="polsos1" href="#"  data-action="open" class="titu1" ><?php echo $lang['nosotros'] ?></h3>
			<h3 id="btn1" data-conima="polsos2" href="#"  data-action="open" class="titu2"><?php echo $lang['contac'] ?></h3>
			
		</div>
	</footer>
	

</body>
</html>