<?php 
session_start ();

if (isset($_POST["lang"])) {
	$lang = $_POST["lang"];
	if(!empty($lang))
		{
		$_SESSION["lang"] = $lang;	
		}
}

if (isset($_SESSION["lang"])) 
{
	$lang= $_SESSION["lang"];
	require "lang/".$lang.".php";
} else {
	require "lang/es.php";
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="<?php echo $lang ['descripcion'] ?>">
	<title><?php echo $lang ["descripcion"] ?></title>
	<link rel="stylesheet" href="">
	<link rel="stylesheet" href="1.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="shorcut icon" type="image/x-icon" href="imagenes/logo.png">
	<script type="text/javascript" src="jquery1.js"></script>
	<script src="javs/cojs.js" type="text/javascript" ></script>
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<script>
		

	</script>
	
 
</head>

<body id="inicio" toppadding="0">
	
	<header class=cabecera>
		<div>
			<a href="inicio.php"><img class="logo"  src="iconos/encabezadologo.png" alt="logotipo"  ></a>
			<div class="icosoc">
				<ul class="redso">

					<li class="icored"><a target="_blank" href="https://www.facebook.com/destinos.tolima?fref=ts"><input type="image" name="redsoc" src="imagenes/facebook2.png" ></a></li>
					<li class="icored2"><input type="image" name="redsoc" src="imagenes/whatsapp.png"></li>
					<li class="icored2"><input type="image" name="redsoc" src="imagenes/gorjeo.png"></li>
				</ul>
			</div>
			<form class="uno" method="POST" >

				<input class="bot" name=lang type="image" src="imagenes/columbia.png" value="es">
				<input class="bot" name=lang type="image" src="imagenes/usa.png" value="en">
				<p class="idi">Idioma</p>
			</form>

		</div>
	</header>

	<div id="menu">

		<ul class="menu">
			<a id="btn1" data-conima="paquenaci" data-tab="paquenac" href="#" data-action="open" class="001"><li><?php echo $lang['paqnac'] ?></li></a>
			<a id="btn1" data-conima="paqueinte" href="#" data-tab="paqueint" data-action="open" class="002" ><li><?php echo $lang['paqint'] ?></li></a>
			<a id="btn1" data-conima="paqueibag" href="#" data-tab="paqueiba" data-action="open" class="003" ><li><?php echo $lang['paqiba'] ?></li></a>
			<a id="btn1" data-conima="alquifinc" href="#" data-tab="alquifin" data-action="open" class="005" ><li><?php echo $lang['alqfin'] ?></li></a>
			<a id="btn1" data-conima="vuehote" href="#" data-tab="vuehot" data-action="open" class="006" ><li><?php echo $lang['vuehot'] ?></li></a>
			<a id="btn1" data-conima="visameri" href="#" data-tab="visamer" data-action="open" class="004" ><li><?php echo $lang['visame'] ?></li></a>
			<a id="btn1" data-conima="vuehote" href="visaca/inicio.php" data-tab="vuehot" data-action="open" class="ultimate" ><li><?php echo $lang['cana'] ?></li></a>
			
		</ul>	

	</div>	

	<div class="tab" id="paquenac" style="display:none" >
			<div a class="ira" >
			<a href="paquetes.php"><?php echo $lang['ir'] ?>
			<div class="iani" style="display:none">
				<?php echo $lang['ir a'] ?> 
			</div>
			</a><i class="fa fa-fighter-jet " id="rapido" data-lado="uno"></i>
		</div>
			<ul id="slider" class="pnac">
				
				<li class="ima" id="muestra" data-tab="img1"   data-action="open">
						<img  class="foto" data-tab="img1" src="imapaqnac/avisball.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['verballe'] ?></h3></div>
				</li>
				<li class="ima" data-tab="img2" data-action="open">
						<img class="foto" id="img2" src="imapaqnac/islpro.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['providencia'] ?></h3></div>
				</li>
				<li class="ima" data-tab="img3" data-action="open">
						<img class="foto" id="img3" src="imapaqnac/lagua.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['guajira'] ?></h3></div>
				</li>
				<li class="ima" data-tab="img4" data-action="open">
						<img class="foto" id="img4" src="imapaqnac/sanand.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['sanandres'] ?></h3></div>
				</li>
				
			</ul>
			<ul id="slider-controls" class="slider-controls">
				<li data-pos='0' id="boto"></li>
				<li data-pos='1' id="boto"></li>
				<li data-pos='2' id="boto"></li>
				<li data-pos='3' id="boto"></li>
			
				
			</ul>
	</div>
	<div id="paquenaci" class="dere" style="display:none">
		
	
		<ul class="der"  id="der1"  data-pos="0" style="">
			<li><img  id="boto" data-pos='0' class="de" src="imapaqnac/avisball.jpg" alt=""></li>
			<li><img  id="boto" data-pos='1' class="de" src="imapaqnac/islpro.jpg" alt=""></li>	
			<li><img  id="boto" data-pos='2' class="de" src="imapaqnac/lagua.jpg" alt=""></li>
			<li><img  id="boto" data-pos='3' class="de" src="imapaqnac/sanand.jpg" alt=""></li>
		</ul>
	</div>
	<div class="tab" id="paqueint" style="display:none">
			<div a class="ira" >
				<a href="paqueteinternacional.php"><?php echo $lang['ir'] ?>
				<div class="iani" style="display:none">
					<?php echo $lang['ir a'] ?> 
				</div>
				</a><i class="fa fa-fighter-jet " id="rapido" data-lado="uno"></i>
			</div>
		<ul id="slider" class="pnac">
				<li class="ima" id="muestra" data-tab="img1"   data-action="open">
						<img class="foto" data-tab="img1" src="imapaqinte/disney.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['disney'] ?></h3></div>
				</li>
				<li class="ima" data-tab="img2" data-action="open">
						<img class="foto" id="img2" src="imapaqinte/cancun1.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['cancun'] ?></h3></div>
				</li>
				<li class="ima" data-tab="img3" data-action="open">
						<img class="foto" id="img3" src="imapaqinte/islamargarita.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['margarita'] ?></h3></div>
				</li>
				
				
			</ul>
			<ul id="slider-controls" class="slider-controls">
				<li data-pos='0' id="boto"></li>
				<li data-pos='1' id="boto"></li>
				<li data-pos='2' id="boto"></li>
				
			
				
			</ul>
	</div>
	<div id="paqueinte" class="dere" style="display:none">
		
	
		<ul class="der12"  id="der1"  data-pos="0" style="">
			<li><img  id="boto" data-pos='0' class="de1" src="imapaqinte/disney.jpg" alt=""></li>
			<li><img  id="boto" data-pos='1' class="de1" src="imapaqinte/cancun1.jpg" alt=""></li>	
			<li><img  id="boto" data-pos='2' class="de1" src="imapaqinte/islamargarita.jpg" alt=""></li>
		</ul>
	</div>
	<div class="tab" id="paqueiba" style="display:none">
		<div a class="ira" >
			<a href="paquetesibague.php"><?php echo $lang['ir'] ?>
			<div class="iani" style="display:none">
				<?php echo $lang['ir a'] ?> 
			</div>
			</a><i class="fa fa-fighter-jet " id="rapido" data-lado="uno"></i>
		</div>
		<ul id="slider" class="pnac">
				<li class="ima" id="muestra" data-tab="img1"   data-action="open">
						<img class="foto" data-tab="img1" src="ibague/ibanat.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['ibaguemusical'] ?></h3></div>
				</li>
				<li class="ima" data-tab="img2" data-action="open">
						<img class="foto" id="img2" src="ibague/canon.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['ibaguenatural'] ?></h3></div>
				</li>
				<li class="ima" data-tab="img3" data-action="open">
						<img class="foto" id="img3" src="ibague/machin.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['cajamarca'] ?></h3></div>
				</li>
				<li class="ima" data-tab="img4" data-action="open">
						<img class="foto" id="img4" src="ibague/turisextre.jpg"  >
						<div class=fincas><h3 class="tit"><?php echo $lang['extremo'] ?></h3></div>
				</li>
				
			</ul>
			<ul id="slider-controls" class="slider-controls">
				<li data-pos='0' id="boto"></li>
				<li data-pos='1' id="boto"></li>
				<li data-pos='2' id="boto"></li>
				<li data-pos='3' id="boto"></li>
			
				
			</ul>
	</div>
	<div id="paqueibag" class="dere" style="display:none">
		
	
		<ul class="der"  id="der1"  data-pos="0" style="">
			<li><img  id="boto" data-pos='0' class="de" src="ibague/ibanat.jpg" alt=""></li>
			<li><img  id="boto" data-pos='1' class="de" src="ibague/canon.jpg" alt=""></li>	
			<li><img  id="boto" data-pos='2' class="de" src="ibague/machin.jpg" alt=""></li>
			<li><img  id="boto" data-pos='3' class="de" src="ibague/turisextre.jpg" alt=""></li>
		</ul>
	</div>
	<div class="tab" id="visamer" style="display:none">
			<div a class="ira" >
				<a href="asesoria-visa-americana/"><?php echo $lang['ir'] ?>
				<div class="iani" style="display:none">
				<?php echo $lang['ir a'] ?> 
				</div>
			</a><i class="fa fa-fighter-jet " id="rapido" data-lado="uno"></i>
			</div>
		<ul id="slider" class="pnac">
				<li class="ima" id="muestra" data-tab="img1"   data-action="open">
						<img class="foto" data-tab="img1" src="visaame/slider-img1.jpg"  >
						<div class="vis">
							<h3 id="visa" ><?php echo $lang['visa1'] ?></h3>
							<p class="visi"><?php echo $lang['visa1.1'] ?></p>
						</div>
				</li>
				<li class="ima" data-tab="img2" data-action="open">
						<img class="foto" id="img2" src="visaame/slider-img2.jpg"  >
						<div class="vis"><h3 id="visa" ><?php echo $lang['visa2'] ?></h3><p class="visi"><?php echo $lang['visa2.1'] ?></p></div>
				</li>
				<li class="ima" data-tab="img3" data-action="open">
						<img class="foto" id="img3" src="visaame/slider-img3.jpg"  >
						<div class="vis"><h3 id="visa" ><?php echo $lang['visa3'] ?></h3><p class="visi"><?php echo $lang['visa3.1'] ?></p></div>
				</li>
				
				
			</ul>
			<ul id="slider-controls" class="slider-controls">
				<li data-pos='0' id="boto"></li>
				<li data-pos='1' id="boto"></li>
				<li data-pos='2' id="boto"></li>
				
			
				
			</ul>
	</div>
	<div class="tab" id="alquifin" style="display:none">
		<div a class="ira" >
			<a href="fincas.php"><?php echo $lang['ir'] ?>
			<div class="iani" style="display:none">
				<?php echo $lang['ir a'] ?> 
			</div>
			</a><i class="fa fa-fighter-jet " id="rapido" data-lado="uno"></i>
		</div>
		<ul id="slider" class="pnac">
				<li class="ima" data-tab="img2" data-action="open">
						<img class="foto" id="img2" src="fincas/refmont.jpg"  >
						<div class=fincas><h3 class="tit">REFUGIO DE LA MONTAÑA</h3></div>
				</li>
				<li class="ima" data-tab="img3" data-action="open">
						<img class="foto" id="img3" src="fincas/villcan.jpg"  >
						<div class=fincas><h3 class="tit">VILLA DEL CANTAR</h3></div>
				</li>
				<li class="ima" data-tab="img4" data-action="open">
						<img class="foto" id="img4" src="fincas/villcar.jpg"  >
						<div class=fincas><h3 class="tit">VILLA CAROLINA</h3></div>
				</li>
				<li class="ima" data-tab="img5" data-action="open">
						<img class="foto" id="img5" src="fincas/villcris.jpg" >
						<div class=fincas><h3 class="tit">VILLA CRISTINA</h3></div>
				</li>
				<li class="ima" data-tab="img6" data-action="open">
						<img class="foto" id="img6" src="fincas/villens.jpg"  >
						<div class=fincas><h3 class="tit">VILLA ENSUEÑO</h3></div>
				</li>
				<li class="ima" data-tab="img7" data-action="open">
						<img class="foto" id="img7" src="fincas/villleo.jpg"  >
						<div class=fincas><h3 class="tit">VILLA LEONES</h3></div>
				</li>
							
			</ul>
			<ul id="slider-controls" class="slider-controls">
				<li data-pos='0' id="boto"></li>
				<li data-pos='1' id="boto"></li>
				<li data-pos='2' id="boto"></li>
				<li data-pos='3' id="boto"></li>
				<li data-pos='4' id="boto"></li>
				<li data-pos='5' id="boto"></li>
				

		<h2 id="titu">opcion 5</h2>
	</div>
	<div id="alquifinc" class="dere" style="display:none">
		
	
		<ul class="der11"  id="der3"  data-pos="0" style="">
			<li><img  id="boto" data-pos='0' class="de" src="fincas/refmont.jpg"  alt=""></li>	
			<li><img  id="boto" data-pos='1' class="de" src="fincas/villcan.jpg"  alt=""></li>
			<li><img  id="boto" data-pos='2' class="de" src="fincas/villcar.jpg"  alt=""></li>
			<li><img  id="boto" data-pos='3' class="de" src="fincas/villcris.jpg"  alt=""></li>
			<li><img  id="boto" data-pos='4' class="de" src="fincas/villens.jpg" alt=""></li>	
			<li><img  id="boto" data-pos='5' class="de" src="fincas/villleo.jpg"  alt=""></li>
			
			
		</ul>
	</div>	
	<div class="tab" id="vuehot" style="display:none">
		<script id="despegar-root-js" type="text/javascript" src="//www.despegar.com.co/comunidadafiliados/widgetGenerator/widgetCore.js" data-id="14851" data-context-path="//www.despegar.com.co/comunidadafiliados"></script>	</div>
	<div id="vuehote" class="dere" style="display:none">
		
	
		<ul class="der"  id="der1"  data-pos="0" style="display:none">
			<li><img  id="boto" data-pos='0' class="de" src="imapaqnac/avisball.jpg" alt=""></li>
			<li><img  id="boto" data-pos='1' class="de" src="imapaqnac/islpro.jpg" alt=""></li>	
			<li><img  id="boto" data-pos='2' class="de" src="imapaqnac/lagua.jpg" alt=""></li>
			<li><img  id="boto" data-pos='3' class="de" src="imapaqnac/sanand.jpg" alt=""></li>
		</ul>
	</div>
	<div class="dere" id="polsos" style="display:none">
		<div class="poli">
		<h3><?php echo $lang['polisos'] ?></h3>
		<p class="politica"><?php echo $lang['politica'] ?>			
		</p>
		</div>
	</div>
	<div class="dere" id="polsos1" style="display:none">
		<div class="poli">
		<h3 class="vi"><?php echo $lang['tituvision'] ?></h3>
		<p class="politica"><?php echo $lang['vision'] ?></p>
		<h3 class="vi"><?php echo $lang['titumision'] ?></h3>
		<p class="politica"><?php echo $lang['mision'] ?></p>

		</div>
	</div>
	<div class="dere" id="polsos2" style="display:none">
		<ul class="contactanos">
			<li><a href="https://www.facebook.com/destinos.tolima?fref=ts" title=""><input class="c" type="image" src="imagenes/facebook.png"><p class="c">Destinos Tolima</p></a></li>
			<li><input class="c" type="image" src="imagenes/tecnologia.png"><p class="c">0573204199366</p></li>
			<li><input class="c" type="image" src="imagenes/gmail-1.png"><p class="c"></p></li>
			<li><a  href=""><input class="c" type="image" src="imagenes/whatsapp-1.png"><p class="c">+0573204199366</p></a></li>
		</ul>
	</div>
	<footer id="pie">
			<img class="icologo" id="avion" data-grado="one" src="imagenes/titulo2.png" alt="logotipo" >
		<div class="pies">
			<h3 id="btn3" data-conima="polsos" href="#"  data-action="open" class="titu0"><?php echo $lang['polisos'] ?></h3>
			
			<h3 id="btn3" data-conima="polsos1" href="#"  data-action="open" class="titu1" ><?php echo $lang['nosotros'] ?></h3>
			<h3 id="btn3" data-conima="polsos2" href="#"  data-action="open" class="titu2"><?php echo $lang['contac'] ?></h3>
			
		</div>
	</footer>


</body>
</html>